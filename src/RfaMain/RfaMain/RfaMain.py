import os
import unittest
import vtk, qt, ctk, slicer
import qSlicerSegmentationsModuleWidgetsPythonQt
import numpy as np
from slicer.ScriptedLoadableModule import *
from slicer.util import VTKObservationMixin
import logging
from segmentationVisualizationWidget import *
from probePlacementWidget import *
from planAnalysisWidget import *


#
# RfaMain
#

class RfaMain(ScriptedLoadableModule):
  """Uses ScriptedLoadableModule base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "RFA Planning" # TODO make this more human readable by adding spaces
    self.parent.categories = ["Radiofrequency Ablation"]
    self.parent.dependencies = []
    self.parent.contributors = ["Grace Underwood (Perk Lab), Cari Whyne (Sunnybrook Research Institute), Michael Hardisty (Sunnybrook Research Institute)"] # replace with "Firstname Lastname (Organization)"
    self.parent.helpText = """
This is slicelet for planning the radiofrequency ablation of vertebral metastases. This module includes importing and setting up of patient imaging, tissue segmentation, and definition of probe parameters and placement."""
    self.parent.helpText += self.getDefaultModuleDocumentationLink()
    self.parent.acknowledgementText = """""" # replace with organization, grant and thanks.
    # Color Scheme: #C7DDF5, #9ACEFF, #669ACC, #336799,, #14293D
    self.styleSheet = """
          QDockWidget:title { background-color: #9ACEFF; }
          QFrame { color: #C7DDF5; background-color: #336799; border-color: #9ACEFF; }
          QGroupBox { color: #C7DDF5; }
          QAbstractButton { color: #14293D; border-style: outset; border-width: 2px; border-radius: 10px; background-color: #C7DDF5; border-color: #9ACEFF; }
          QAbstractButton:pressed { background-color: #9ACEFF; }
          QAbstractButton:checked { background-color: #669ACC; }
          QAbstractButton:disabled { background-color: #C0C0C0; }
          """

#
# RfaMainWidget
#

class RfaMainWidget(ScriptedLoadableModuleWidget):
  """Uses ScriptedLoadableModuleWidget base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent = None):
    ScriptedLoadableModuleWidget.__init__(self, parent)

    self.logic = RfaMainLogic()

    if not parent:
      self.parent = slicer.qMRMLWidget()
      self.parent.setLayout(qt.QVBoxLayout())
      self.parent.setMRMLScene(slicer.mrmlScene)
    else:
      self.parent = parent

    self.layout = self.parent.layout()
    if not parent:
      self.setup()
      self.parent.show()

    self.roiNode = None
    self.planVolumeNode = None

  def setup(self):
    ScriptedLoadableModuleWidget.setup(self)

    # Instantiate and connect widgets ...

    #
    # Launcher Panel
    #
    launcherCollapsibleButton = ctk.ctkCollapsibleButton()
    launcherCollapsibleButton.text = "Slicelet Launcher"
    self.layout.addWidget(launcherCollapsibleButton)
    launcherFormLayout = qt.QFormLayout(launcherCollapsibleButton)

    informationLabel = qt.QLabel('Ensure that the following extensions have been installed: \n - Segmentation Editor Extra Effects \n - SlicerRT')
    launcherFormLayout.addWidget(informationLabel)

    # Add slicelet button
    self.launchSliceletButton = qt.QPushButton("Start RFA Planning")
    self.launchSliceletButton.toolTip = "Launch the RFA planning application"
    launcherFormLayout.addWidget(self.launchSliceletButton)
    self.launchSliceletButton.connect('clicked()', self.showFullScreen)

    self.layout.addStretch(1)

    self.createMainMenuDockWidget()
    self.segmentationVisualizationWidget = segmentationVisualizationWidget(self)
    self.segmentationVisualizationWidget.createDockWidget()
    self.segmentationVisualizationDockWidget = self.segmentationVisualizationWidget.sliceletDockWidget

    self.probePlacementWidget = probePlacementWidget(self)
    self.probePlacementWidget.createDockWidget()
    self.probePlacementDockWidget = self.probePlacementWidget.sliceletDockWidget

    self.planAnalysisWidget = planAnalysisWidget(self)
    self.planAnalysisWidget.createDockWidget()
    self.planAnalysisDockWidget = self.planAnalysisWidget.sliceletDockWidget

  def createMainMenuDockWidget(self):
    self.mainMenuDockWidget = qt.QDockWidget(self.parent)
    self.mainMenuDockWidget.setObjectName('Import Patient Data')
    self.mainMenuDockWidget.setWindowTitle('Import Patient Data')
    self.mainMenuDockWidget.setStyleSheet(slicer.modules.RfaMainInstance.styleSheet)
    self.mainMenuDockWidget.setFeatures(qt.QDockWidget.DockWidgetMovable | qt.QDockWidget.DockWidgetFloatable)

    mainWindow = slicer.util.mainWindow()
    self.mainMenuDockWidget.setParent(mainWindow)

    self.sliceletPanel = qt.QFrame(self.mainMenuDockWidget)
    self.sliceletpanelLayout = qt.QVBoxLayout(self.sliceletPanel)
    self.mainMenuDockWidget.setWidget(self.sliceletPanel)

    #
    # Step 1: Load and prep patient data
    #
    self.loadDataCollapsibleButton = ctk.ctkCollapsibleButton()
    self.loadDataCollapsibleButton.text = "1. Import Patient Data"
    self.sliceletpanelLayout.addWidget(self.loadDataCollapsibleButton)
    self.loadDataLayout = qt.QVBoxLayout(self.loadDataCollapsibleButton)
    loadDataLabel = qt.QLabel('1. Use the load buttons to import patient scans. \n2. Select the correct patient volume. \n3. Next crop the scans to the vertebrae of interest using the ROI annotator. \n4. When you are satisfied with the defined region crop the volume.\n5. Then select the [TISSUE SEGMENTATION] tab below.')
    self.loadDataLayout.addWidget(loadDataLabel)

    self.loadDataButtonsLayout = qt.QHBoxLayout()
    self.loadDataLayout.addLayout(self.loadDataButtonsLayout)

    self.loadDICOMButton = qt.QPushButton("Load DICOM")
    self.loadDataButtonsLayout.addWidget(self.loadDICOMButton)
    self.loadDICOMButton.connect('clicked()', self.onLoadDICOM)

    self.loadNonDICOMButton = qt.QPushButton("Load Other")
    self.loadDataButtonsLayout.addWidget(self.loadNonDICOMButton)
    self.loadNonDICOMButton.connect('clicked()', self.onLoadNonDICOM)

    self.planVolumeSelector = slicer.qMRMLNodeComboBox()
    self.planVolumeSelector.nodeTypes = ['vtkMRMLScalarVolumeNode']
    self.planVolumeSelector.selectNodeUponCreation = True
    self.planVolumeSelector.addEnabled = False
    self.planVolumeSelector.removeEnabled = True
    self.planVolumeSelector.renameEnabled = True
    self.planVolumeSelector.noneEnabled = False
    self.planVolumeSelector.setMRMLScene(slicer.mrmlScene)
    self.planVolumeSelector.setToolTip('Select CT image for planning')
    self.planVolumeSelector.connect('currentNodeChanged(vtkMRMLNode*)', self.onSelect)
    self.loadDataLayout.addWidget(self.planVolumeSelector)

    self.displayROIButton = qt.QCheckBox("Display ROI Annotator")
    self.displayROIButton.name = "Display ROI"
    self.loadDataLayout.addWidget(self.displayROIButton)
    self.displayROIButton.connect('clicked()', self.onDisplayROI)

    self.cropVolumeButton = qt.QPushButton("Crop Volume")
    self.loadDataLayout.addWidget(self.cropVolumeButton)
    self.cropVolumeButton.connect('clicked()', self.onCrop)

    self.sliceletpanelLayout.addStretch(1)

    self.exitApplication = qt.QPushButton("Exit")
    self.exitApplication.toolTip = "Exit the application"
    self.exitApplication.name = "exitApplication"
    self.sliceletpanelLayout.addWidget(self.exitApplication)
    self.exitApplication.connect('clicked()', self.onExit)

    self.advancedCollapsibleButton = ctk.ctkCollapsibleButton()
    self.advancedCollapsibleButton.setProperty('collapsedHeight', 4)
    self.advancedCollapsibleButton.setProperty('collapsed', True)
    self.advancedCollapsibleButton.text = "Advanced settings"
    self.sliceletpanelLayout.addWidget(self.advancedCollapsibleButton)

    self.advancedCollapsibleLayout = qt.QFormLayout(self.advancedCollapsibleButton)
    self.advancedCollapsibleLayout.setContentsMargins(12, 4, 4, 4)
    self.advancedCollapsibleLayout.setSpacing(4)

    self.showFullScreenButton = qt.QPushButton()
    self.showFullScreenButton.setText("Show SpineNav only")
    self.advancedCollapsibleLayout.addRow(self.showFullScreenButton)
    self.showFullScreenButton.connect('clicked()', self.showFullScreen)

    self.showFullSlicerInterfaceButton = qt.QPushButton()
    self.showFullSlicerInterfaceButton.setText("Show full user interface")
    self.advancedCollapsibleLayout.addRow(self.showFullSlicerInterfaceButton)
    self.showFullSlicerInterfaceButton.connect('clicked()', self.onShowFullSlicerInterface)

    self.showPythonConsoleButton = qt.QPushButton()
    self.showPythonConsoleButton.setText("Show Python console")
    self.advancedCollapsibleLayout.addRow(self.showPythonConsoleButton)
    self.showPythonConsoleButton.connect('clicked()', self.showPythonConsole)

    self.showApplicationLogButton = qt.QPushButton()
    self.showApplicationLogButton.setText("Show application log")
    self.advancedCollapsibleLayout.addRow(self.showApplicationLogButton)
    self.showApplicationLogButton.connect('clicked()', self.showApplicationLog)

  def onSelect(self):
    self.planVolumeNode = self.planVolumeSelector.currentNode()
    layoutManager = slicer.app.layoutManager()
    redSliceWidget = layoutManager.sliceWidget("Red")
    yellowSliceWidget = layoutManager.sliceWidget("Yellow")
    greenSliceWidget = layoutManager.sliceWidget("Green")
    redLogic = redSliceWidget.sliceLogic()
    yellowLogic = yellowSliceWidget.sliceLogic()
    greenLogic = greenSliceWidget.sliceLogic()
    redLogic.GetSliceCompositeNode().SetBackgroundVolumeID(self.planVolumeNode.GetID())
    yellowLogic.GetSliceCompositeNode().SetBackgroundVolumeID(self.planVolumeNode.GetID())
    greenLogic.GetSliceCompositeNode().SetBackgroundVolumeID(self.planVolumeNode.GetID())
    segmentationNodes = slicer.util.getNodesByClass("vtkMRMLSegmentationNode")
    for segmentNode in segmentationNodes:
      segmentNode.SetDisplayVisibility(0)
      print("Done")

  """Opens the DICOM browser"""
  def onLoadDICOM(self):
    self.dicomWidget = slicer.modules.dicom.widgetRepresentation().self()
    self.dicomWidget.detailsPopup.open()
    self.planVolumeSelector.setCurrentNodeIndex(0)
    slicer.app.layoutManager().threeDWidget(0).threeDView().resetCamera()
    segmentationNodes = slicer.util.getNodesByClass("vtkMRMLSegmentationNode")
    for segmentNode in segmentationNodes:
      segmentNode.SetDisplayVisibility(0)
      print("Done")
    slicer.app.layoutManager().activeMRMLThreeDViewNode().SetBoxVisible(0)

  """Open the file browser to add non-DICOM objects"""
  def onLoadNonDICOM(self):
    #Since not a DICOM then create a patient hierarchy
    subjectHierarchyNode = slicer.vtkMRMLSubjectHierarchyNode().GetSubjectHierarchyNode(slicer.mrmlScene)
    subjectID = subjectHierarchyNode.CreateSubjectItem(subjectHierarchyNode.GetSceneItemID(), "patient")
    studyID = subjectHierarchyNode.CreateStudyItem(subjectID, "Name")
    loaded = slicer.util.openAddDataDialog()
    self.planVolumeSelector.setCurrentNodeIndex(0)
    slicer.app.layoutManager().threeDWidget(0).threeDView().resetCamera()
    slicer.app.layoutManager().activeMRMLThreeDViewNode().SetBoxVisible(0)
    segmentationNodes = slicer.util.getNodesByClass("vtkMRMLSegmentationNode")
    for segmentNode in segmentationNodes:
      segmentNode.SetDisplayVisibility(0)
    volumeID = subjectHierarchyNode.GetItemByDataNode(self.planVolumeSelector.currentNode())
    subjectHierarchyNode.SetItemParent(volumeID, studyID)

  """Create Region of Interest box to defien cropped region"""
  def onDisplayROI(self):
    self.planVolumeNode = self.planVolumeSelector.currentNode()
    displayROI = self.displayROIButton.checked
    self.roiNode = self.logic.visualizeROI(displayROI, self.planVolumeNode, self.roiNode)

  def onCrop(self):
    self.planVolumeNode = self.planVolumeSelector.currentNode()
    if self.planVolumeNode == None:
      messagebox = qt.QMessageBox()
      messagebox.setText("There is no input volume.")
      messagebox.exec_()
    if self.roiNode == None:
      messagebox = qt.QMessageBox()
      messagebox.setText("There is no ROI annotator")
      messagebox.exec_()

    self.outputVolumeNode = slicer.vtkMRMLScalarVolumeNode()
    self.outputVolumeNode.SetName("cropped_volume")
    slicer.mrmlScene.AddNode(self.outputVolumeNode)

    # Create Cropped Volume
    cropVolumeNode = slicer.vtkMRMLCropVolumeParametersNode()
    cropVolumeNode.SetInputVolumeNodeID(self.planVolumeNode.GetID())
    cropVolumeNode.SetOutputVolumeNodeID(self.outputVolumeNode.GetID())
    cropVolumeNode.SetROINodeID(self.roiNode.GetID())
    cropVolumeNode.SetIsotropicResampling(1)

    slicer.modules.cropvolume.logic().Apply(cropVolumeNode)

    self.roiNode.SetDisplayVisibility(0)
    self.displayROIButton.checked = 0

    self.planVolumeSelector.setCurrentNodeID(self.outputVolumeNode.GetID())

    self.setBackground(self.planVolumeSelector.currentNode())

  def setBackground(self, volumeNode):
    layoutManager = slicer.app.layoutManager()
    redSlice = layoutManager.sliceWidget("Red")
    redLogic = redSlice.sliceLogic().GetSliceCompositeNode().SetBackgroundVolumeID(volumeNode.GetID())
    yellowSlice = layoutManager.sliceWidget("Yellow")
    yellowLogic = yellowSlice.sliceLogic().GetSliceCompositeNode().SetBackgroundVolumeID(volumeNode.GetID())
    greenSlice = layoutManager.sliceWidget("Green")
    greenLogic = greenSlice.sliceLogic().GetSliceCompositeNode().SetBackgroundVolumeID(volumeNode.GetID())

  def onExit(self):
    slicer.util.mainWindow().close()

  def showPythonConsole(self):
    mainWindow = slicer.util.mainWindow()
    mainWindow.on_WindowPythonInteractorAction_triggered()

  def showApplicationLog(self):
    mainWindow = slicer.util.mainWindow()
    mainWindow.on_HelpReportBugOrFeatureRequestAction_triggered()

  def showFullScreen(self):

    # We hide all toolbars, etc. which is inconvenient as a default startup setting,
    # therefore disable saving of window setup.
    settings = qt.QSettings()
    settings.setValue('MainWindow/RestoreGeometry', 'false')

    self.showToolbars(False)
    self.showModulePanel(False)
    self.showMenuBar(False)
    self.showFullSliceViewGui(False)

    mainWindow = slicer.util.mainWindow()
    mainWindow.addDockWidget(qt.Qt.LeftDockWidgetArea, self.mainMenuDockWidget)
    #mainWindow.tabifyDockWidget(self.mainMenuDockWidget)

    self.mainMenuDockWidget.show()

    self.mainMenuDockWidget.raise_()

    mainWindow.tabifyDockWidget(self.mainMenuDockWidget, self.segmentationVisualizationDockWidget)
    mainWindow.tabifyDockWidget(self.mainMenuDockWidget, self.probePlacementDockWidget)
    mainWindow.tabifyDockWidget(self.mainMenuDockWidget, self.planAnalysisDockWidget)

    mainWindow.setWindowTitle('RFA')
    mainWindow.showMaximized()

    # Hide slice view annotations (patient name, scale, color bar, etc.)
    import DataProbe
    dataProbeUtil = DataProbe.DataProbeLib.DataProbeUtil()
    dataProbeParameterNode = dataProbeUtil.getParameterNode()
    dataProbeParameterNode.SetParameter('showSliceViewAnnotations', '0')

  def onShowFullSlicerInterface(self):
    self.showToolbars(True)
    self.showModulePanel(True)
    self.showMenuBar(True)
    self.showFullSliceViewGui(True)
    slicer.util.mainWindow().showMaximized()

    # Save current state
    settings = qt.QSettings()
    settings.setValue('MainWindow/RestoreGeometry', 'true')

  def showToolbars(self, show):
    for toolbar in slicer.util.mainWindow().findChildren('QToolBar'):
      toolbar.setVisible(show)

  def showModulePanel(self, show):
    modulePanelDockWidget = slicer.util.mainWindow().findChildren('QDockWidget', 'PanelDockWidget')[0]
    modulePanelDockWidget.setVisible(show)
    if show:
      mainWindow = slicer.util.mainWindow()
      mainWindow.tabifyDockWidget(self.mainMenuDockWidget, modulePanelDockWidget)

  def showMenuBar(self, show):
    for menubar in slicer.util.mainWindow().findChildren('QMenuBar'):
      menubar.setVisible(show)

  def showFullSliceViewGui(self, show):
    redSlice = slicer.app.layoutManager().sliceWidget("Red")
    self.showViewerPinButton(redSlice, show)
    yellowSlice = slicer.app.layoutManager().sliceWidget("Yellow")
    self.showViewerPinButton(yellowSlice, show)
    greenSlice = slicer.app.layoutManager().sliceWidget("Green")
    self.showViewerPinButton(greenSlice, show)
    threeDWidget = slicer.app.layoutManager().threeDWidget(0)
    self.showViewerPinButton(threeDWidget, show)

  def showViewerPinButton(self, sliceWidget, show):
    try:
      sliceControlWidget = sliceWidget.children()[1]
      pinButton = sliceControlWidget.children()[1].children()[1]
      if show:
        pinButton.show()
      else:
        pinButton.hide()
    except:
      pass

  def cleanup(self):
    pass


#
# RfaMainLogic
#

class RfaMainLogic(ScriptedLoadableModuleLogic):
  """This class should implement all the actual
  computation done by your module.  The interface
  should be such that other python code can import
  this class and make use of the functionality without
  requiring an instance of the Widget.
  Uses ScriptedLoadableModuleLogic base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def visualizeROI(self,ROIEnabled, volumeNode, ROINode):
    if ROIEnabled:
      if ROINode == None:
        ROINode = slicer.vtkMRMLAnnotationROINode()
        ROINode.SetDisplayVisibility(ROIEnabled)
        ROINode.SetName("ROI")
        ROINode.SetRadiusXYZ([20,20,20])
        redSlice = slicer.app.layoutManager().sliceWidget("Red")
        redLogic = redSlice.sliceLogic()
        volumeBounds = [0, 0, 0, 0, 0, 0]
        redLogic.GetSliceBounds(volumeBounds)
        x = (volumeBounds[1] + volumeBounds[0]) / -2
        y = (volumeBounds[3] + volumeBounds[2]) / 2
        z = (volumeBounds[5] + volumeBounds[4]) / 2
        ROINode.SetXYZ([x,y,z])
        slicer.mrmlScene.AddNode(ROINode)
        return ROINode
      else:
        slicer.mrmlScene.RemoveNode(ROINode)
        ROINode = slicer.vtkMRMLAnnotationROINode()
        ROINode.SetDisplayVisibility(ROIEnabled)
        ROINode.SetName("ROI")
        ROINode.SetRadiusXYZ([20, 20, 20])
        redSlice = slicer.app.layoutManager().sliceWidget("Red")
        redLogic = redSlice.sliceLogic()
        volumeBounds = [0, 0, 0, 0, 0, 0]
        redLogic.GetSliceBounds(volumeBounds)
        x = (volumeBounds[1] + volumeBounds[0]) / -2
        y = (volumeBounds[3] + volumeBounds[2]) / 2
        z = (volumeBounds[5] + volumeBounds[4]) / 2
        ROINode.SetXYZ([x, y, z])
        slicer.mrmlScene.AddNode(ROINode)
        return ROINode




class RfaMainTest(ScriptedLoadableModuleTest):
  """
  This is the test case for your scripted module.
  Uses ScriptedLoadableModuleTest base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setUp(self):
    """ Do whatever is needed to reset the state - typically a scene clear will be enough.
    """
    slicer.mrmlScene.Clear(0)

  def runTest(self):
    """Run as few or as many tests as needed here.
    """
    self.setUp()
    self.test_RfaMain1()

  def test_RfaMain1(self):
    """ Ideally you should have several levels of tests.  At the lowest level
    tests should exercise the functionality of the logic with different inputs
    (both valid and invalid).  At higher levels your tests should emulate the
    way the user would interact with your code and confirm that it still works
    the way you intended.
    One of the most important features of the tests is that it should alert other
    developers when their changes will have an impact on the behavior of your
    module.  For example, if a developer removes a feature that you depend on,
    your test should break so they know that the feature is needed.
    """

    self.delayDisplay("Starting the test")
    #
    # first, get some data
    #
    import urllib
    downloads = (
        ('http://slicer.kitware.com/midas3/download?items=5767', 'FA.nrrd', slicer.util.loadVolume),
        )

    for url,name,loader in downloads:
      filePath = slicer.app.temporaryPath + '/' + name
      if not os.path.exists(filePath) or os.stat(filePath).st_size == 0:
        logging.info('Requesting download %s from %s...\n' % (name, url))
        urllib.urlretrieve(url, filePath)
      if loader:
        logging.info('Loading %s...' % (name,))
        loader(filePath)
    self.delayDisplay('Finished with download and loading')

    volumeNode = slicer.util.getNode(pattern="FA")
    logic = RfaMainLogic()
    self.assertIsNotNone( logic.hasImageData(volumeNode) )
    self.delayDisplay('Test passed!')
