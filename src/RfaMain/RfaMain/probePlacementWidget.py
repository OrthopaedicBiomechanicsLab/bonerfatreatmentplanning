import logging
import math
import os
import time
import unittest
import numpy as np
from __main__ import vtk, qt, ctk, slicer
from RfaMain import *

class probePlacementWidget():
  def __init__(self, parent=None):

    self.parent = parent
    self.probeModel = None

    self.VIEW_MODE_TABBED_SLICE = 0
    self.VIEW_MODE_FOUR_UP = 1
    self.VIEW_MODE_CONVENTIONAL = 2
    self.VIEW_MODE_ONE_UP_3D = 3

    self.probeModel = None
    self.targetFiducialList = None
    self.entryFiducialList = None

    self.MOUSE_MODE_IDLE = 0
    self.MOUSE_MODE_TIP = 1
    self.MOUSE_MODE_ENTRY = 2

    self.logic = probePlacementLogic()

  def createDockWidget(self):
    self.sliceletDockWidget = qt.QDockWidget(self.parent)
    self.sliceletDockWidget.setObjectName('Probe Parameters')
    self.sliceletDockWidget.setWindowTitle('Probe Parameters')
    self.sliceletDockWidget.setStyleSheet(slicer.modules.RfaMainInstance.styleSheet)
    self.sliceletDockWidget.setFeatures(qt.QDockWidget.DockWidgetMovable | qt.QDockWidget.DockWidgetFloatable)  # not closable

    mainWindow = slicer.util.mainWindow()
    self.sliceletDockWidget.setParent(mainWindow)

    self.sliceletPanel = qt.QFrame(self.sliceletDockWidget)
    self.layout = qt.QVBoxLayout(self.sliceletPanel)
    self.sliceletDockWidget.setWidget(self.sliceletPanel)


    #self.layout.addStretch(0.5)

    self.probeParametersCollapsibleButton = ctk.ctkCollapsibleButton()
    self.probeParametersCollapsibleButton.text = "3. Probe Parameters"

    self.layout.addWidget(self.probeParametersCollapsibleButton)

    self.probeParametersCollapseLayout = qt.QVBoxLayout(self.probeParametersCollapsibleButton)

    self.displayLayoutBox = qt.QGroupBox('Display Layout')
    self.displayLayout = qt.QGridLayout()
    self.displayLayoutBox.setLayout(self.displayLayout)

    self.probeParametersCollapseLayout.addWidget(self.displayLayoutBox)

    self.displayTabbedSliceButton = qt.QPushButton("Slice View")
    self.displayTabbedSliceButton.name = "Tabbed Slice"
    self.displayTabbedSliceButton.setCheckable(True)
    self.displayTabbedSliceButton.setFlat(True)
    self.displayLayout.addWidget(self.displayTabbedSliceButton, 0, 0)
    self.displayTabbedSliceButton.connect('clicked()', lambda: self.onViewMode(self.VIEW_MODE_TABBED_SLICE))  # Write the function on view mode where 0 value is slices

    self.displayFourButton = qt.QPushButton("Four up")
    self.displayFourButton.name = "Four up"
    self.displayFourButton.setCheckable(True)
    self.displayFourButton.setFlat(True)
    self.displayLayout.addWidget(self.displayFourButton, 0, 1)
    self.displayFourButton.connect('clicked()', lambda: self.onViewMode(self.VIEW_MODE_FOUR_UP))  # Write function on view mode where four up is 1

    self.displayConventionalButton = qt.QPushButton("Conventional")
    self.displayConventionalButton.name = "Conventional"
    self.displayConventionalButton.setCheckable(True)
    self.displayConventionalButton.setFlat(True)
    self.displayLayout.addWidget(self.displayConventionalButton, 1, 0)
    self.displayConventionalButton.connect('clicked()', lambda: self.onViewMode(self.VIEW_MODE_CONVENTIONAL))  # Write the function on view mode where conventional is 2

    self.display3DButton = qt.QPushButton("3D only")
    self.display3DButton.name = "3D Only"
    self.display3DButton.setCheckable(True)
    self.display3DButton.setFlat(True)
    self.displayLayout.addWidget(self.display3DButton, 1, 1)
    self.display3DButton.connect('clicked()', lambda: self.onViewMode(self.VIEW_MODE_ONE_UP_3D))  # Write the function on view mode where 3 is 3d

    self.probeParametersGroupBox = qt.QGroupBox('Probe Parameters')
    self.probeParametersLayout = qt.QFormLayout()
    self.probeParametersGroupBox.setLayout(self.probeParametersLayout)

    self.probePlacementLabel = qt.QLabel('1. Click the [ADD PROBE MODEL] button. \n2. Set the different probe parameters, the default time is 15 mins. \n3. Select the location of the probe tip and entry. \n4. Select the file destination')
    self.probeParametersCollapseLayout.addWidget(self.probePlacementLabel)

    self.probeParametersCollapseLayout.addWidget(self.probeParametersGroupBox)

    #add a label that says the default time is set to 15 minutes
    #check with michael and sean to confirm that the start time should always be zero?

    self.addProbeModelButton = qt.QPushButton('Add Probe Model')
    self.addProbeModelButton.name = "addProbeModel"
    self.probeParametersLayout.addRow(self.addProbeModelButton)
    self.addProbeModelButton.connect('clicked()', self.onAddProbe)

    self.probeTime = qt.QSpinBox()
    self.probeTime.minimum = 0
    self.probeTime.maximum = 900
    self.probeTime.suffix = 's'
    self.probeTime.setValue(900)
    self.probeParametersLayout.addRow("Ablation Time: ", self.probeTime)

    self.probePower = qt.QSpinBox()
    self.probePower.minimum = -1
    self.probePower.maximum = 15
    self.probePower.suffix = 'W'
    self.probePower.setValue(5)
    self.probeParametersLayout.addRow("Power: ", self.probePower)

    self.probeCoolantTemp = qt.QSpinBox()
    self.probeCoolantTemp.minimum = -1
    self.probeCoolantTemp.maximum = 15
    self.probeCoolantTemp.suffix = 'C'
    self.probeCoolantTemp.setValue(15)
    self.probeParametersLayout.addRow("Coolant Temperature: ", self.probeCoolantTemp)

    self.probePlacementGroupBox = qt.QGroupBox('Probe Parameters')
    self.probePlacementLayout = qt.QGridLayout()
    self.probePlacementGroupBox.setLayout(self.probePlacementLayout)

    self.probeParametersCollapseLayout.addWidget(self.probePlacementGroupBox)

    self.placeTipButton = qt.QPushButton("Place Probe Tip")
    self.placeTipButton.name = "ProbeTip"
    self.placeTipButton.setCheckable(True)
    self.placeTipButton.setFlat(True)
    self.probePlacementLayout.addWidget(self.placeTipButton, 0,0)
    self.placeTipButton.connect('clicked()', self.onPlaceTip)

    self.placeEntryButton = qt.QPushButton("Place Probe Entry")
    self.placeEntryButton.name = "ProbeEntry"
    self.placeEntryButton.setCheckable(True)
    self.placeEntryButton.setFlat(True)
    self.probePlacementLayout.addWidget(self.placeEntryButton, 0,1)
    self.placeEntryButton.connect('clicked()', self.onPlaceEntry)

    self.deleteTargetFiducialsButton = qt.QPushButton("Delete Probe Tip")
    self.probePlacementLayout.addWidget(self.deleteTargetFiducialsButton, 1,0)
    self.deleteTargetFiducialsButton.connect("clicked()", self.onDeleteTargetFiducials)

    self.deleteEntryFiducialsbutton = qt.QPushButton("Delete Probe Entry")
    self.probePlacementLayout.addWidget(self.deleteEntryFiducialsbutton, 1,1)
    self.deleteEntryFiducialsbutton.connect("clicked()", self.onDeleteEntryFiducials)

    self.opacitySlider = ctk.ctkSliderWidget()
    self.opacitySlider.minimum=0
    self.opacitySlider.maximum=1
    self.opacitySlider.value = 1
    self.opacitySlider.singleStep=0.1
    self.opacitySlider.connect("valueChanged(double)", self.onOpacityChanged)
    self.probeParametersCollapseLayout.addWidget(self.opacitySlider)

    self.planFileGroupBox = qt.QGroupBox('RFA Plan File')
    self.planFileLayout = qt.QHBoxLayout()
    self.planFileGroupBox.setLayout(self.planFileLayout)

    self.probeParametersCollapseLayout.addWidget(self.planFileGroupBox)

    self.selectDestinationLabel = qt.QLabel('Select file destination: ')
    self.planFileLayout.addWidget(self.selectDestinationLabel)

    self.planFileDestinationSelector = ctk.ctkPathLineEdit()
    self.planFileDestinationSelector.filters = ctk.ctkPathLineEdit.Dirs
    self.planFileDestinationSelector.options = ctk.ctkPathLineEdit.DontUseSheet
    self.planFileDestinationSelector.options = ctk.ctkPathLineEdit.ShowDirsOnly
    self.planFileDestinationSelector.showHistoryButton = False
    self.planFileLayout.addWidget(self.planFileDestinationSelector)

    self.createFileButton = qt.QPushButton("Create RFA File")
    self.createFileButton.name = "CreateFileButton"
    self.createFileButton.setCheckable(True)
    self.createFileButton.setFlat(True)
    self.planFileLayout.addWidget(self.createFileButton)
    self.createFileButton.connect('clicked()', self.onCreateFile)

    self.settingsGroupBox = qt.QGroupBox('Settings')
    self.settingsLayout = qt.QFormLayout()
    self.settingsGroupBox.setLayout(self.settingsLayout)

    self.probeParametersCollapseLayout.addWidget(self.settingsGroupBox)

    self.showMainSlicerButton = qt.QPushButton('Slicer Main')
    self.showMainSlicerButton.setText('Navigate out of module and back to Main Slicer Window')
    self.settingsLayout.addRow(self.showMainSlicerButton)
    self.showMainSlicerButton.connect('clicked()', self.onShowMainSlicer)

  def onOpacityChanged(self):
    bone = slicer.util.getFirstNodeByClassByName('vtkMRMLModelNode', 'bone')
    boneModelDisplay = bone.GetDisplayNode()
    boneModelDisplay.SetOpacity(self.opacitySlider.value)
    print("Opacity Changed")
  def onShowMainSlicer(self):
    for toolbar in slicer.util.mainWindow().findChildren('QToolBar'):
      toolbar.setVisible(True)

    modulePanelDockWidget = slicer.util.mainWindow().findChildren('QDockWidget', 'PanelDockWidget')[0]
    modulePanelDockWidget.setVisible(True)
    if True:
      mainWindow = slicer.util.mainWindow()
      mainWindow.tabifyDockWidget(self.mainMenuDockWidget, modulePanelDockWidget)

    for menubar in slicer.util.mainWindow().findChildren('QMenuBar'):
      menubar.setVisible(True)

    redSlice = slicer.app.layoutManager().sliceWidget("Red")
    self.showViewerPinButton(redSlice, True)
    yellowSlice = slicer.app.layoutManager().sliceWidget("Yellow")
    self.showViewerPinButton(yellowSlice, True)
    greenSlice = slicer.app.layoutManager().sliceWidget("Green")
    self.showViewerPinButton(greenSlice, show)
    threeDWidget = slicer.app.layoutManager().threeDWidget(0)
    self.showViewerPinButton(threeDWidget, True)

    slicer.util.mainWindow().showMaximized()

    # Save current state
    settings = qt.QSettings()
    settings.setValue('MainWindow/RestoreGeometry', 'true')

  def showViewerPinButton(self, sliceWidget, show):
    try:
      sliceControlWidget = sliceWidget.children()[1]
      pinButton = sliceControlWidget.children()[1].children()[1]
      if show:
        pinButton.show()
      else:
        pinButton.hide()
    except:
      pass

  def onCreateFile(self):
    currentPath = self.planFileDestinationSelector.currentPath
    fileName = 'patient_output.txt'
    completeName = os.path.join(currentPath, fileName)
    outputFile = open(completeName, "w")
    outputFile.write('<NUM_DEVICES> 1' + '\n')
    outputFile.write('<PROBE_POWER_PARAMS>' + '\n')
    outputFile.write('<POWER_IS_CONSTANT> false' + '\n')
    outputFile.write('<START_TIME> 0' + '\n')
    outputFile.write('<POWER_PULSE_ARRAY> 1' + '\n')
    outputFile.write(
      str(0) + ' ' + str(self.probeTime.value) + ' ' + str(self.probePower.value) + '\n')
    outputFile.write('<POWER_PARAM_END>' + '\n')
    outputFile.write('<PROBE_POSITION>' + '\n')
    target = [0, 0, 0]
    self.targetFiducialList.GetNthFiducialPosition(0, target)
    outputFile.write(str(target[0]) + ' ' + str(target[1]) + ' ' + str(target[2]) + '\n')
    outputFile.write('<PROBE_ORIENTATION>' + '\n')
    entry = [0, 0, 0]
    self.entryFiducialList.GetNthFiducialPosition(0, entry)
    orientation = [-(target[0] - entry[0]), -(target[1] - entry[1]), -(target[2] - entry[2])]
    orientation = orientation / np.linalg.norm(orientation)
    outputFile.write(str(orientation[0]) + ' ' + str(orientation[1]) + ' ' + str(orientation[2]) + '\n')
    outputFile.write('<COOLANT_TEMPERATURE> ' + str(self.probeCoolantTemp.value) + '\n')
    outputFile.write('<PROBE_END>' + '\n')

    outputFile.close()

  def onAddProbe(self):
    if self.probeModel == None:
      rfaProbeModelPath = os.path.join(os.path.dirname(__file__), 'RFAProbe.ply')
      rfaProbeModel = slicer.modules.models.logic().AddModel(rfaProbeModelPath)
      self.probeModel = slicer.util.getNode('RFAProbe')
      probeDisplayNode = self.probeModel.GetDisplayNode()
      probeDisplayNode.SetColor(0,1,1)
      probeDisplayNode.SetSliceIntersectionVisibility(1)
      self.probeModelTransform = slicer.vtkMRMLTransformNode()
      self.probeModelTransform.SetName("ProbeModelTransform")
      slicer.mrmlScene.AddNode(self.probeModelTransform)
      self.probeModel.SetAndObserveTransformNodeID(self.probeModelTransform.GetID())

  def onViewMode(self, mode):
    displayLayoutManager = slicer.app.layoutManager()
    if mode == self.VIEW_MODE_TABBED_SLICE:
      displayLayoutManager.layout = slicer.vtkMRMLLayoutNode.SlicerLayoutTabbedSliceView
      self.displayTabbedSliceButton.setChecked(True)
      self.displayFourButton.setChecked(False)
      self.displayConventionalButton.setChecked(False)
      self.display3DButton.setChecked(False)
    elif mode == self.VIEW_MODE_FOUR_UP:
      displayLayoutManager.layout = slicer.vtkMRMLLayoutNode.SlicerLayoutFourUpView
      self.displayTabbedSliceButton.setChecked(False)
      self.displayFourButton.setChecked(True)
      self.displayConventionalButton.setChecked(False)
      self.display3DButton.setChecked(False)
    elif mode == self.VIEW_MODE_CONVENTIONAL:
      displayLayoutManager.layout = slicer.vtkMRMLLayoutNode.SlicerLayoutConventionalView
      self.displayTabbedSliceButton.setChecked(False)
      self.displayFourButton.setChecked(False)
      self.displayConventionalButton.setChecked(True)
      self.display3DButton.setChecked(False)
    elif mode == self.VIEW_MODE_ONE_UP_3D:
      displayLayoutManager.layout = slicer.vtkMRMLLayoutNode.SlicerLayoutOneUp3DView
      self.displayTabbedSliceButton.setChecked(False)
      self.displayFourButton.setChecked(False)
      self.displayConventionalButton.setChecked(False)
      self.display3DButton.setChecked(True)

  def onPlaceTip(self):
    if self.targetFiducialList == None:
      self.targetFiducialList = slicer.vtkMRMLMarkupsFiducialNode()
      self.targetFiducialList.SetName("targetLocation")
      slicer.mrmlScene.AddNode(self.targetFiducialList)
      targetDisplay = self.targetFiducialList.GetDisplayNode()
      targetDisplay.SetSelectedColor([0,0,1])
      targetDisplay.SetGlyphScale(5)
      targetDisplay.SetTextScale(2)
      tag = self.targetFiducialList.AddObserver(vtk.vtkCommand.ModifiedEvent, self.onTargetFiducialModified)
      self.setCurrentMouseMode(self.MOUSE_MODE_TIP)

  def onPlaceEntry(self):
    if self.entryFiducialList == None:
      self.entryFiducialList = slicer.vtkMRMLMarkupsFiducialNode()
      self.entryFiducialList.SetName("entryLocation")
      slicer.mrmlScene.AddNode(self.entryFiducialList)
      entryDisplay = self.entryFiducialList.GetDisplayNode()
      entryDisplay.SetSelectedColor([0,0,1])
      entryDisplay.SetGlyphScale(5)
      entryDisplay.SetTextScale(2)
      tag = self.entryFiducialList.AddObserver(vtk.vtkCommand.ModifiedEvent, self.onEntryFiducialModified)
      self.setCurrentMouseMode(self.MOUSE_MODE_ENTRY)

  def onDeleteTargetFiducials(self):
    identity = vtk.vtkMatrix4x4()
    self.probeModelTransform.SetMatrixTransformToParent(identity)
    if self.targetFiducialList != None:
      self.targetFiducialList.RemoveAllMarkups()

  def onDeleteEntryFiducials(self):
    identity = vtk.vtkMatrix4x4()
    self.probeModelTransform.SetMatrixTransformToParent(identity)
    if self.entryFiducialList != None:
      self.entryFiducialList.RemoveAllMarkups()

  def onTargetFiducialModified(self, observer, eventId):
    self.probeModelTransform, updated = self.logic.updateTransform(self.targetFiducialList, self.entryFiducialList, self.probeModelTransform)
    if updated == True:
      self.updateSliceView()

  def onEntryFiducialModified(self, observer, eventId):
    self.probeModelTransform, updated = self.logic.updateTransform(self.targetFiducialList, self.entryFiducialList, self.probeModelTransform)
    if updated == True:
      self.updateSliceView()


  def setCurrentMouseMode(self, mode):
    selectionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLSelectionNodeSingleton")
    interactionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLInteractionNodeSingleton")
    if mode == self.MOUSE_MODE_TIP:
      selectionNode.SetReferenceActivePlaceNodeClassName("vtkMRMLMarkupsFiducialNode")
      selectionNode.SetActivePlaceNodeID(self.targetFiducialList.GetID())
      placeModePersistance= 0
      slicer.modules.markups.logic().StartPlaceMode(placeModePersistance)
      self.placeTipButton.setChecked(True)
      self.placeEntryButton.setChecked(False)
    elif mode == self.MOUSE_MODE_ENTRY:
      selectionNode.SetReferenceActivePlaceNodeClassName("vtkMRMLMarkupsFiducialNode")
      selectionNode.SetActivePlaceNodeID(self.entryFiducialList.GetID())
      placeModePersistance = 0
      slicer.modules.markups.logic().StartPlaceMode(placeModePersistance)
      self.placeTipButton.setChecked(False)
      self.placeEntryButton.setChecked(True)
    else:
      interactionNode.SwitchToViewTransformMode()
      self.placeEntryButton.setChecked(False)
      self.placeTipButton.setChecked(False)

  def updateSliceView(self):
    probeToWorldTransformMatrix = vtk.vtkMatrix4x4()
    self.probeModelTransform.GetMatrixTransformToWorld(probeToWorldTransformMatrix)
    yellowSlice = slicer.app.layoutManager().sliceWidget("Yellow")
    yellowController = yellowSlice.sliceController()
    yellowController.setSliceOffsetValue(probeToWorldTransformMatrix.GetElement(0,3))

    greenSlice = slicer.app.layoutManager().sliceWidget("Green")
    greenController = greenSlice.sliceController()
    greenController.setSliceOffsetValue(probeToWorldTransformMatrix.GetElement(1, 3))

    redSlice = slicer.app.layoutManager().sliceWidget("Red")
    redController = redSlice.sliceController()
    redController.setSliceOffsetValue(probeToWorldTransformMatrix.GetElement(2, 3))

    #slicer.app.layoutManager().threeDWidget(0).threeDView().resetFocalPoint()
    #slicer.app.layoutManager().threeDWidget(0).threeDView().resetCamera()

class probePlacementLogic():
  def updateTransform(self, targetFiducialList, entryFiducialList, probeModelTransform):

    if targetFiducialList != None and entryFiducialList != None:
      targetPoint = [0, 0, 0]
      entryPoint = [0, 0, 0]

      targetFiducialList.GetNthFiducialPosition(0, targetPoint)
      entryFiducialList.GetNthFiducialPosition(0, entryPoint)

      if targetPoint != [0,0,0] and entryPoint != [0,0,0]:
        zVector = [targetPoint[0]-entryPoint[0], targetPoint[1]-entryPoint[1], targetPoint[2]-entryPoint[2]]
        xVector = [1,0,0]
        yVector = np.cross(zVector,xVector)
        xVector = np.cross(yVector,zVector)
        zVector = zVector/np.linalg.norm(zVector)
        yVector = yVector / np.linalg.norm(yVector)
        xVector = xVector / np.linalg.norm(xVector)

        matrix = vtk.vtkMatrix4x4()
        probeModelTransform.GetMatrixTransformToParent(matrix)
        matrix.SetElement(0, 0, xVector[0])
        matrix.SetElement(1, 0, xVector[1])
        matrix.SetElement(2, 0, xVector[2])
        matrix.SetElement(0, 1, yVector[0])
        matrix.SetElement(1, 1, yVector[1])
        matrix.SetElement(2, 1, yVector[2])
        matrix.SetElement(0, 2, zVector[0])
        matrix.SetElement(1, 2, zVector[1])
        matrix.SetElement(2, 2, zVector[2])
        matrix.SetElement(0, 3, targetPoint[0])
        matrix.SetElement(1, 3, targetPoint[1])
        matrix.SetElement(2, 3, targetPoint[2])
        probeModelTransform.SetMatrixTransformToParent(matrix)
        return probeModelTransform, True
    else:
      matrix = vtk.vtkMatrix4x4()
      probeModelTransform.GetMatrixTransformToParent(matrix)
      probeModelTransform.SetMatrixTransformToParent(matrix)
      return probeModelTransform, False



