import logging
import math
import os
import time
import unittest
from __main__ import vtk, qt, ctk, slicer
from RfaMain import *


class segmentationVisualizationWidget():
  def __init__(self, parent=None):

    self.parent = parent
    #self.logic = segmentationVisualizationLogic()

    self.parameterSetNode = None
    self.segmentationNode = None
    self.masterVolume = None


  def createDockWidget(self):
    self.sliceletDockWidget = qt.QDockWidget(self.parent)
    self.sliceletDockWidget.setObjectName('Tissue Segmentation')
    self.sliceletDockWidget.setWindowTitle('Tissue Segmentation')
    self.sliceletDockWidget.setStyleSheet(slicer.modules.RfaMainInstance.styleSheet)
    self.sliceletDockWidget.setFeatures(qt.QDockWidget.DockWidgetMovable | qt.QDockWidget.DockWidgetFloatable)  # not closable

    mainWindow = slicer.util.mainWindow()
    self.sliceletDockWidget.setParent(mainWindow)

    self.sliceletPanel = qt.QFrame(self.sliceletDockWidget)
    self.layout = qt.QVBoxLayout(self.sliceletPanel)
    self.sliceletDockWidget.setWidget(self.sliceletPanel)

    self.segmentationGroupBox = qt.QGroupBox('2. Segmentation and Visualization')

    self.segmentationLayout = qt.QVBoxLayout()
    self.segmentationGroupBox.setLayout(self.segmentationLayout)
    self.layout.addWidget(self.segmentationGroupBox)

    self.informationLabel = qt.QLabel('1. First press the [START SEGMENTATION] button to create initial segmentations.\n2.Edit the initial segmentations as desired.\n3.Use the [GROW FROM SEEDS] or [WATERSHED] effects to ensure all voxels are classified.\n4.Do not forget to apply the individual effects being used\n5. Click the [FINISH SEGMENTATION] button when finished.')
    self.segmentationLayout.addWidget(self.informationLabel)

    self.startSegmentationButton = qt.QPushButton("Start Segmentation")
    self.segmentationLayout.addWidget(self.startSegmentationButton)
    self.startSegmentationButton.connect('clicked()', self.onSegmentation)

    self.applySegmentationButton = qt.QPushButton("Finish Segmentation")
    self.segmentationLayout.addWidget(self.applySegmentationButton)
    self.applySegmentationButton.connect('clicked()', self.onApplySegmentation)

    import qSlicerSegmentationsModuleWidgetsPythonQt
    self.editor = qSlicerSegmentationsModuleWidgetsPythonQt.qMRMLSegmentEditorWidget()
    self.editor.setMaximumNumberOfUndoStates(10)
    self.selectParameterNode()
    self.editor.setMRMLScene(slicer.mrmlScene)

    self.segmentationLayout.addWidget(self.editor)


  def getMasterVolume(self):
    layoutManager = slicer.app.layoutManager()
    redSlice = layoutManager.sliceWidget("Red")
    volumeNodeID = redSlice.sliceLogic().GetSliceCompositeNode().GetBackgroundVolumeID()
    volumeNode = slicer.mrmlScene.GetNodeByID(volumeNodeID)
    return volumeNode

  def onSegmentation(self):
    if len(slicer.util.getNodesByClass("vtkMRMLScalarVolumeNode")) > 0:
      if self.segmentationNode == None:

        self.segmentationNode = slicer.vtkMRMLSegmentationNode()
        slicer.mrmlScene.AddNode(self.segmentationNode)
        self.segmentationNode.CreateDefaultDisplayNodes()
        self.masterVolume = self.getMasterVolume()
        scalarRange = self.masterVolume.GetImageData().GetScalarRange()
        self.segmentationNode.SetReferenceImageGeometryParameterFromVolumeNode(self.masterVolume)
        self.boneSegment = self.segmentationNode.GetSegmentation().AddEmptySegment('bone', 'bone', [1,1,0.6])

        self.editorNode = slicer.mrmlScene.AddNewNodeByClass("vtkMRMLSegmentEditorNode")
        self.editor.setMRMLSegmentEditorNode(self.editorNode)
        self.editor.setSegmentationNode(self.segmentationNode)
        self.editor.setMasterVolumeNode(self.masterVolume)

        self.editor.setCurrentSegmentID(self.boneSegment)
        self.editor.setActiveEffectByName('Threshold')
        effect = self.editor.activeEffect()
        effect.setParameter('MinimumThreshold', '200')
        effect.setParameter('MaximumThreshold', str(scalarRange[1]))
        effect.self().onApply()

        self.tissueSegment = self.segmentationNode.GetSegmentation().AddEmptySegment('tissue', 'tissue',[1,0.2,0.46])
        self.editor.setCurrentSegmentID(self.tissueSegment)
        self.editor.setActiveEffectByName('Threshold')
        effect = self.editor.activeEffect()
        effect.setParameter('MinimumThreshold', '40')
        effect.setParameter('MaximumThreshold', '80')
        effect.self().onApply()

        self.fatSegment = self.segmentationNode.GetSegmentation().AddEmptySegment('fat', 'fat', [0, 0.9, 0])
        self.editor.setCurrentSegmentID(self.fatSegment)
        self.editor.setActiveEffectByName('Threshold')
        effect = self.editor.activeEffect()
        effect.setParameter('MinimumThreshold', '-100')
        effect.setParameter('MaximumThreshold', '-60')
        effect.self().onApply()

        self.airSegment = self.segmentationNode.GetSegmentation().AddEmptySegment('air', 'air', [0.4,0.8,1])
        self.editor.setCurrentSegmentID(self.airSegment)
        self.editor.setActiveEffectByName('Threshold')
        effect = self.editor.activeEffect()
        effect.setParameter('MinimumThreshold', str(scalarRange[0]))
        effect.setParameter('MaximumThreshold', '-150')
        effect.self().onApply()

  def selectParameterNode(self):
    # Select parameter set node if one is found in the scene, and create one otherwise
    segmentEditorSingletonTag = "SegmentEditor"
    segmentEditorNode = slicer.mrmlScene.GetSingletonNode(segmentEditorSingletonTag, "vtkMRMLSegmentEditorNode")
    if segmentEditorNode is None:
      segmentEditorNode = slicer.vtkMRMLSegmentEditorNode()
      segmentEditorNode.SetSingletonTag(segmentEditorSingletonTag)
      segmentEditorNode = slicer.mrmlScene.AddNode(segmentEditorNode)
    if self.parameterSetNode == segmentEditorNode:
      # nothing changed
      return
    self.parameterSetNode = segmentEditorNode
    self.editor.setMRMLSegmentEditorNode(self.parameterSetNode)

  def getCompositeNode(self, layoutName):
    """ use the Red slice composite node to define the active volumes """
    count = slicer.mrmlScene.GetNumberOfNodesByClass('vtkMRMLSliceCompositeNode')
    for n in xrange(count):
      compNode = slicer.mrmlScene.GetNthNodeByClass(n, 'vtkMRMLSliceCompositeNode')
      if layoutName and compNode.GetLayoutName() != layoutName:
        continue
      return compNode

  def getDefaultMasterVolumeNodeID(self):
    layoutManager = slicer.app.layoutManager()
    # Use first background volume node in any of the displayed layouts
    for layoutName in layoutManager.sliceViewNames():
      compositeNode = self.getCompositeNode(layoutName)
      if compositeNode.GetBackgroundVolumeID():
        return compositeNode.GetBackgroundVolumeID()
    # Use first background volume node in any of the displayed layouts
    for layoutName in layoutManager.sliceViewNames():
      compositeNode = self.getCompositeNode(layoutName)
      if compositeNode.GetForegroundVolumeID():
        return compositeNode.GetForegroundVolumeID()
    # Not found anything
    return None

  def onApplySegmentation(self):
    exportedModelsNode = slicer.mrmlScene.AddNewNodeByClass('vtkMRMLModelHierarchyNode')
    segId = vtk.vtkStringArray()
    segId.InsertNextValue('bone')
    print(segId)
    slicer.modules.segmentations.logic().ExportSegmentsToModelHierarchy(self.segmentationNode, segId, exportedModelsNode)
    boneModel = slicer.util.getFirstNodeByClassByName('vtkMRMLModelNode', 'bone')
    boneModelDisplay = boneModel.GetDisplayNode()
    boneModelDisplay.SetOpacity(1.0)
    self.segmentationNode.RemoveClosedSurfaceRepresentation()
    self.labelMap = slicer.mrmlScene.AddNewNodeByClass('vtkMRMLLabelMapVolumeNode')
    slicer.modules.segmentations.logic().ExportAllSegmentsToLabelmapNode(self.segmentationNode, self.labelMap)
    self.segmentationNode.SetDisplayVisibility(0)