import os
import unittest
import time
import math
import qSlicerSegmentationsModuleWidgetsPythonQt
import numpy as np

from __main__ import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *
from slicer.util import VTKObservationMixin


import logging

#
# SpineMain
#

class RFAPlanning(ScriptedLoadableModule):
  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)

    parent.title = "Spinal RFA Planning"
    parent.categories = ["Radiofrequency Ablation"]
    parent.dependencies = []
    parent.contributors = ["Grace Underwood (Perk Lab)"]
    parent.helpText = "Spinal fusion application main menu"
    parent.acknowledgementText = ""
    self.parent = parent
        
    # Color scheme: #C7DDF5, #9ACEFF, #669ACC, #336799,, #14293D
    self.styleSheet = """
      QDockWidget:title { background-color: #93EDD9; }
      QFrame { color: #CBEFEA; background-color: #3D7267; border-color: #93EDD9; }
      QGroupBox { color: #CBEFEA; }
      QAbstractButton { color: #14293D; border-style: outset; border-width: 2px; border-radius: 10px; background-color: #CBEFEA; border-color: #93EDD9; }
      QAbstractButton:pressed { background-color: #93EDD9; }
      QAbstractButton:checked { background-color: #68B7A6; }
      QAbstractButton:disabled { background-color: #C0C0C0; }
      """

#
# SpineMainWidget - standard Slicer module widget
#
class RFAPlanningWidget(ScriptedLoadableModuleWidget):

  def __init__(self, parent = None):
    ScriptedLoadableModuleWidget.__init__(self, parent)

    self.logic = RFAPlanningLogic()
    
    if not parent:
      self.parent = slicer.qMRMLWidget()
      self.parent.setLayout(qt.QVBoxLayout())
      self.parent.setMRMLScene(slicer.mrmlScene)
    else:
      self.parent = parent

    self.layout = self.parent.layout()
    if not parent:
      self.setup()
      self.parent.show()

    self.VIEW_MODE_TABBED_SLICE = 0
    self.VIEW_MODE_FOUR_UP = 1
    self.VIEW_MODE_CONVENTIONAL = 2
    self.VIEW_MODE_ONE_UP_3D = 3

    self.MOUSE_MODE_IDLE = 0
    self.MOUSE_MODE_PLACEMENT_TIP = 1
    self.MOUSE_MODE_PLACEMENT_ENTRY = 2
    self.MOUSE_MODE_PLACEMENT = 4

    self.probeModel = None

    self.targetFiducialList = None
    self.entryFiducialList = None

    self.probeTransform = None
    self.inputVolumeNode = None
    self.roiNode = None
    self.outputVolumeNode = None
    self.segmentationNode = None
      

  def __del__(self):
    self.cleanup()

  def cleanup(self):
   
    self.launchSliceletButton.disconnect('clicked()', self.showFullScreen)
    
    self.exitApplication.disconnect('clicked()', self.onExit)
    self.showFullSlicerInterfaceButton.disconnect('clicked()', self.onShowFullSlicerInterface)
    # TODO: add missing disconnects
    
  def setup(self):
    # Regular module setup
    ScriptedLoadableModuleWidget.setup(self)

    # Launcher panel
    launcherCollapsibleButton = ctk.ctkCollapsibleButton()
    launcherCollapsibleButton.text = "Slicelet launcher"
    self.layout.addWidget(launcherCollapsibleButton)
    launcherFormLayout = qt.QFormLayout(launcherCollapsibleButton)

    # Show slicelet button
    self.launchSliceletButton = qt.QPushButton("Start Spine Navigation")
    self.launchSliceletButton.toolTip = "Launch the Spine Navigation application"
    launcherFormLayout.addWidget(self.launchSliceletButton)
    self.launchSliceletButton.connect('clicked()', self.showFullScreen)
    
    self.layout.addStretch(1)

    self.createMainMenuDockWidget() 
    
    qt.QTimer.singleShot(100, self.showFullScreen)
    
  def createMainMenuDockWidget(self):
    # Create Slicelet Widget

    logging.debug('SpineMainSlicelet.__init__')
    
    # Set up main frame.

    self.mainMenuDockWidget = qt.QDockWidget(self.parent)
    self.mainMenuDockWidget.setObjectName('SpinalRFAMain')
    self.mainMenuDockWidget.setWindowTitle('Main')
    
    self.mainMenuDockWidget.setStyleSheet(slicer.modules.RFAPlanningInstance.styleSheet)
    self.mainMenuDockWidget.setFeatures(qt.QDockWidget.DockWidgetMovable | qt.QDockWidget.DockWidgetFloatable) # not closable
    
    mainWindow=slicer.util.mainWindow()
    self.mainMenuDockWidget.setParent(mainWindow)
            
    self.sliceletPanel = qt.QFrame(self.mainMenuDockWidget)
    self.sliceletPanelLayout = qt.QVBoxLayout(self.sliceletPanel)    
    self.mainMenuDockWidget.setWidget(self.sliceletPanel)

    self.displayLayoutBox = qt.QGroupBox('DisplayLayout')
    displayLayout = qt.QGridLayout()
    self.displayLayoutBox.setLayout(displayLayout)
    self.sliceletPanelLayout.addWidget(self.displayLayoutBox)

    self.displayTabbedSliceButton = qt.QPushButton("Slice View")
    self.displayTabbedSliceButton.name = "Tabbed Slice"
    self.displayTabbedSliceButton.setCheckable(True)
    self.displayTabbedSliceButton.setFlat(True)
    displayLayout.addWidget(self.displayTabbedSliceButton, 0,0)
    self.displayTabbedSliceButton.connect('clicked()', lambda: self.onViewMode(self.VIEW_MODE_TABBED_SLICE)) #Write the function on view mode where 0 value is slices

    self.displayFourButton = qt.QPushButton("Four up")
    self.displayFourButton.name = "Four up"
    self.displayFourButton.setCheckable(True)
    self.displayFourButton.setFlat(True)
    displayLayout.addWidget(self.displayFourButton, 0,1)
    self.displayFourButton.connect('clicked()', lambda: self.onViewMode(self.VIEW_MODE_FOUR_UP)) #Write function on view mode where four up is 1

    self.displayConventionalButton = qt.QPushButton("Conventional")
    self.displayConventionalButton.name = "Conventional"
    self.displayConventionalButton.setCheckable(True)
    self.displayConventionalButton.setFlat(True)
    displayLayout.addWidget(self.displayConventionalButton, 1,0)
    self.displayConventionalButton.connect('clicked()', lambda: self.onViewMode(self.VIEW_MODE_CONVENTIONAL)) #Write the function on view mode where conventional is 2

    self.display3DButton = qt.QPushButton("3D only")
    self.display3DButton.name = "3D Only"
    self.display3DButton.setCheckable(True)
    self.display3DButton.setFlat(True)
    displayLayout.addWidget(self.display3DButton, 1,1)
    self.display3DButton.connect('clicked()', lambda: self.onViewMode(self.VIEW_MODE_ONE_UP_3D))  #Write the function on view mode where 3 is 3d
    
    #
    # Step 1 loading in data
    #
    
    self.loadDataCollapsibleButton = ctk.ctkCollapsibleButton()
    self.loadDataCollapsibleButton.text = "1. Load Data"
    self.sliceletPanelLayout.addWidget(self.loadDataCollapsibleButton)

    self.loadDataLayout = qt.QVBoxLayout(self.loadDataCollapsibleButton)

    self.loadDataButtonsLayout = qt.QHBoxLayout()
    self.loadDataLayout.addLayout(self.loadDataButtonsLayout)

    self.loadDICOMButton = qt.QPushButton("Load DICOM")
    self.loadDataButtonsLayout.addWidget(self.loadDICOMButton)
    self.loadDICOMButton.connect('clicked()', self.onLoadDICOM)

    self.loadNonDICOMButton = qt.QPushButton("Load Other")
    self.loadDataButtonsLayout.addWidget(self.loadNonDICOMButton)
    self.loadNonDICOMButton.connect('clicked()', self.onLoadNonDICOM)

    self.planVolumeSelector = slicer.qMRMLNodeComboBox()
    self.planVolumeSelector.nodeTypes = ['vtkMRMLScalarVolumeNode']
    self.planVolumeSelector.selectNodeUponCreation = True
    self.planVolumeSelector.addEnabled = False
    self.planVolumeSelector.removeEnabled = True
    self.planVolumeSelector.renameEnabled = True
    self.planVolumeSelector.noneEnabled = True
    self.planVolumeSelector.removeEnabled = True
    self.planVolumeSelector.setMRMLScene(slicer.mrmlScene)
    self.planVolumeSelector.setToolTip('Select CT image for planning')
    self.loadDataLayout.addWidget(self.planVolumeSelector)

    self.displayROIButton = qt.QCheckBox("Display ROI Annotator")
    self.displayROIButton.name = "Display ROI"
    self.loadDataLayout. addWidget(self.displayROIButton)
    self.displayROIButton.connect('clicked()', self.onDisplayROI)

    self.cropVolumeButton = qt.QPushButton("Crop Volume")
    self.loadDataLayout.addWidget(self.cropVolumeButton)
    self.cropVolumeButton.connect('clicked()', self.onCrop)

    #
    # Step 2 Segmentation of data
    #
    self.segmentationCollapsibleButton = ctk.ctkCollapsibleButton()
    self.segmentationCollapsibleButton.text = "2. Segmentation"
    self.segmentationCollapsibleButton.setProperty('collapsed', True)
    self.segmentationCollapsibleButton.connect('clicked()', self.onSegmentation)
    self.sliceletPanelLayout.addWidget(self.segmentationCollapsibleButton)

    self.segmentationLayout = qt.QVBoxLayout(self.segmentationCollapsibleButton)
    self.parameterSetNode = None

    import qSlicerSegmentationsModuleWidgetsPythonQt
    self.editor = qSlicerSegmentationsModuleWidgetsPythonQt.qMRMLSegmentEditorWidget()
    self.editor.setMaximumNumberOfUndoStates(10)
    # Set parameter node first so that the automatic selections made when the scene is set are saved
    self.selectParameterNode()
    self.editor.setMRMLScene(slicer.mrmlScene)


    self.segmentationLayout.addWidget(self.editor)

    #
    # Step 3 Probe Parameters
    #
    self.probeParameterCollapsibleButton = ctk.ctkCollapsibleButton()
    self.probeParameterCollapsibleButton.text = "3. Probe Parameters & Orientation"
    self.probeParameterCollapsibleButton.setProperty('collapsed', True)
    self.probeParameterCollapsibleButton.connect('clicked()', self.onProbeParameter)
    self.sliceletPanelLayout.addWidget(self.probeParameterCollapsibleButton)

    self.parametersLayout = qt.QFormLayout(self.probeParameterCollapsibleButton)

    self.placeTipButton = qt.QCheckBox("Place Tip")
    self.placeTipButton.name = "Place Tip"
    self.parametersLayout.addRow(self.placeTipButton)
    self.placeTipButton.connect('clicked(bool)', self.onPlacement)
    self.placeTipButton.checked = 0

    self.placeEntryButton = qt.QCheckBox("Place Entry")
    self.placeEntryButton.name = "Place Entry"
    self.parametersLayout.addRow(self.placeEntryButton)
    self.placeEntryButton.connect('clicked(bool)', self.onPlacement)
    self.placeEntryButton.checked = 0

    self.probeStartTime = qt.QSpinBox()
    self.probeStartTime.minimum = -1
    self.probeStartTime.maximum = 900
    self.probeStartTime.suffix = 's'
    self.probeStartTime.setValue(0)
    self.parametersLayout.addRow("Start Time: ", self.probeStartTime)

    self.probeEndTime = qt.QSpinBox()
    self.probeEndTime.minimum = -1
    self.probeEndTime.maximum = 1800
    self.probeEndTime.suffix = 's'
    self.probeEndTime.setValue(1)
    self.parametersLayout.addRow("End Time: ", self.probeEndTime)

    self.probePower = qt.QSpinBox()
    self.probePower.minimum = -1
    self.probePower.maximum = 15
    self.probePower.suffix = 'W'
    self.probePower.setValue(1)
    self.parametersLayout.addRow("Power: ", self.probePower)

    self.probeCoolantTemp = qt.QSpinBox()
    self.probeCoolantTemp.minimum = -1
    self.probeCoolantTemp.maximum = 15
    self.probeCoolantTemp.suffix = 'C'
    self.probeCoolantTemp.setValue(1)
    self.parametersLayout.addRow("Coolant Temperature: ", self.probeCoolantTemp)

    self.deleteFiducialsButton = qt.QPushButton("Delete Target Fiducials")
    self.parametersLayout.addRow(self.deleteFiducialsButton)
    self.deleteFiducialsButton.connect("clicked()", self.onDeleteTargetFiducials)

    self.deleteEntryFiducialsbutton = qt.QPushButton("Delete Entry Fiducials")
    self.parametersLayout.addRow(self.deleteEntryFiducialsbutton)
    self.deleteEntryFiducialsbutton.connect("clicked()", self.onDeleteEntryFiducials)

    #
    # Step 4: Confirm location
    #

    self.textFileCollapsibleButton = ctk.ctkCollapsibleButton()
    self.textFileCollapsibleButton.text = "4. Confirm Location"
    self.textFileCollapsibleButton.setProperty('collapsed', True)
    self.sliceletPanelLayout.addWidget(self.textFileCollapsibleButton)

    self.textLayout = qt.QVBoxLayout(self.textFileCollapsibleButton)
    self.fileLocationLayout = qt.QHBoxLayout()
    self.textLayout.addLayout(self.fileLocationLayout)

    self.selectLocationLabel = qt.QLabel("Select Location: ")
    self.fileLocationLayout.addWidget(self.selectLocationLabel)

    self.documentLocationName = ctk.ctkPathLineEdit()
    self.documentLocationName.filters = ctk.ctkPathLineEdit.Dirs
    self.documentLocationName.options = ctk.ctkPathLineEdit.DontUseSheet
    self.documentLocationName.options = ctk.ctkPathLineEdit.ShowDirsOnly
    self.documentLocationName.showHistoryButton =  False
    self.fileLocationLayout.addWidget(self.documentLocationName)

    self.applyButton = qt.QPushButton("Apply")
    self.textLayout.addWidget(self.applyButton)
    self.applyButton.connect("clicked()", self.onApply)

    # Create GUI panels.
    
    self.sliceletPanelLayout.addStretch(1)
    
    self.exitApplication = qt.QPushButton("Exit")
    self.exitApplication.toolTip = "Exit the application"
    self.exitApplication.name = "exitApplication"
    self.sliceletPanelLayout.addWidget(self.exitApplication)
    self.exitApplication.connect('clicked()', self.onExit)

    # Advanced panel
    
    self.advancedCollapsibleButton = ctk.ctkCollapsibleButton()
    self.advancedCollapsibleButton.setProperty('collapsedHeight', 4)
    self.advancedCollapsibleButton.setProperty('collapsed', True)
    self.advancedCollapsibleButton.text = "Advanced settings"

    self.sliceletPanelLayout.addWidget(self.advancedCollapsibleButton)

    self.advancedCollapsibleLayout = qt.QFormLayout(self.advancedCollapsibleButton)
    self.advancedCollapsibleLayout.setContentsMargins(12,4,4,4)
    self.advancedCollapsibleLayout.setSpacing(4)
    
    self.showFullScreenButton = qt.QPushButton()
    self.showFullScreenButton.setText("Show SpineNav only")
    self.advancedCollapsibleLayout.addRow(self.showFullScreenButton)
    self.showFullScreenButton.connect('clicked()', self.showFullScreen)

    self.showFullSlicerInterfaceButton = qt.QPushButton()
    self.showFullSlicerInterfaceButton.setText("Show full user interface")
    self.advancedCollapsibleLayout.addRow(self.showFullSlicerInterfaceButton)
    self.showFullSlicerInterfaceButton.connect('clicked()', self.onShowFullSlicerInterface)

    self.showPythonConsoleButton = qt.QPushButton()
    self.showPythonConsoleButton.setText("Show Python console")
    self.advancedCollapsibleLayout.addRow(self.showPythonConsoleButton)
    self.showPythonConsoleButton.connect('clicked()', self.showPythonConsole)

    self.showApplicationLogButton = qt.QPushButton()
    self.showApplicationLogButton.setText("Show application log")
    self.advancedCollapsibleLayout.addRow(self.showApplicationLogButton)
    self.showApplicationLogButton.connect('clicked()', self.showApplicationLog)
    

  def onCrop(self):
    self.inputVolumeNode = slicer.util.getNode(self.planVolumeSelector.currentNode().GetID())
    if self.inputVolumeNode == None:
      messagebox = qt.QMessageBox()
      messagebox.setText("There is no input volume.")
      messagebox.exec_()
    if self.roiNode == None:
      messagebox = qt.QMessageBox()
      messagebox.setText("There is no ROI node.")
      messagebox.exec_()
    self.outputVolumeNode = slicer.vtkMRMLScalarVolumeNode()
    self.outputVolumeNode.SetName("cropped_volume")
    slicer.mrmlScene.AddNode(self.outputVolumeNode)


    #Create Cropped Volume
    cropVolumeNode = slicer.vtkMRMLCropVolumeParametersNode()
    cropVolumeNode.SetInputVolumeNodeID(self.inputVolumeNode.GetID())
    cropVolumeNode.SetOutputVolumeNodeID(self.outputVolumeNode.GetID())
    cropVolumeNode.SetROINodeID(self.roiNode.GetID())
    cropVolumeNode.SetIsotropicResampling(1)

    slicer.modules.cropvolume.logic().Apply(cropVolumeNode)

    self.roiNode.SetDisplayVisibility(0)
    self.displayROIButton.checked = 0

    layoutManager = slicer.app.layoutManager()
    redSlice = layoutManager.sliceWidget("Red")
    redLogic = redSlice.sliceLogic().GetSliceCompositeNode().SetBackgroundVolumeID(self.outputVolumeNode.GetID())
    yellowSlice = layoutManager.sliceWidget("Yellow")
    yellowLogic = yellowSlice.sliceLogic().GetSliceCompositeNode().SetBackgroundVolumeID(self.outputVolumeNode.GetID())
    greenSlice = layoutManager.sliceWidget("Green")
    greenLogic = greenSlice.sliceLogic().GetSliceCompositeNode().SetBackgroundVolumeID(self.outputVolumeNode.GetID())
    

  def onDisplayROI(self):
    if self.roiNode == None:
      self.roiNode = slicer.vtkMRMLAnnotationROINode()
      self.roiNode.SetDisplayVisibility(self.displayROIButton.checked)
      self.roiNode.SetName("ROI")
      self.roiNode.SetRadiusXYZ([20,20,20])
      redSlice = slicer.app.layoutManager().sliceWidget("Red")
      redLogic = redSlice.sliceLogic()
      volumeBounds = [0,0,0,0,0,0]
      redLogic.GetSliceBounds(volumeBounds)
      x = (volumeBounds[1] + volumeBounds[0])/-2
      y = (volumeBounds[3] + volumeBounds[2])/2
      z = (volumeBounds[5] + volumeBounds[4])/2
      self.roiNode.SetXYZ([x,y,z])
      slicer.mrmlScene.AddNode(self.roiNode)
    else:
      self.roiNode.Reset()

    

  def onApply(self):
    #having permission errors
    currentPath = self.documentLocationName.label
    print(currentPath)
    fileName = 'patient_output.txt'
    completeName = os.path.join(currentPath,fileName)
    outputFile = open(fileName, 'w')
    outputFile.write('<NUM_DEVICES> 1' + '\n')
    outputFile.write('<PROBE_POWER_PARAMS>' + '\n')
    outputFile.write('<POWER_IS_CONSTANT> false' + '\n')
    outputFile.write('<START_TIME> ' + str(self.probeStartTime.value) + '\n')
    outputFile.write('<POWER_PULSE_ARRAY> 1' + '\n')
    outputFile.write(str(self.probeStartTime.value) + ' ' + str(self.probeEndTime.value) + ' ' + str(self.probePower.value) + '\n')
    outputFile.write('<POWER_PARAM_END>' + '\n')
    outputFile.write('<PROBE_POSITION>' + '\n')
    target = [0,0,0]
    self.targetFiducialList.GetNthFiducialPosition(0, target)
    outputFile.write(str(target[0]) + ' ' + str(target[1]) + ' ' + str(target[2]) + '\n')
    outputFile.write('<PROBE_ORIENTATION>' + '\n')
    entry = [0, 0, 0]
    self.entryFiducialList.GetNthFiducialPosition(0, entry)
    orientation = [(target[0]-entry[0]), (target[1]-entry[1]), (target[2]-entry[2])]
    orientation = orientation/np.linalg.norm(orientation)
    outputFile.write(str(orientation[0]) + ' ' + str(orientation[1]) + ' ' + str(orientation[2]) + '\n')
    outputFile.write('<COOLANT_TEMPERATURE> ' + str(self.probeCoolantTemp.value) + '\n')
    outputFile.write('<PROBE_END>' + '\n')

    outputFile.close()

  def onSegmentation(self):
    if len(slicer.util.getNodesByClass("vtkMRMLScalarVolumeNode")) > 0:
      if self.segmentationNode == None:

        self.segmentationNode = slicer.vtkMRMLSegmentationNode()
        slicer.mrmlScene.AddNode(self.segmentationNode)
        self.segmentationNode.CreateDefaultDisplayNodes()
        self.masterVolume = slicer.util.getNodesByClass("vtkMRMLScalarVolumeNode")[-1]
        scalarRange = self.masterVolume.GetImageData().GetScalarRange()
        self.segmentationNode.SetReferenceImageGeometryParameterFromVolumeNode(self.masterVolume)
        self.boneSegment = self.segmentationNode.GetSegmentation().AddEmptySegment('bone', 'bone', [1,1,0.6])

        self.editorNode = slicer.mrmlScene.AddNewNodeByClass("vtkMRMLSegmentEditorNode")
        self.editor.setMRMLSegmentEditorNode(self.editorNode)
        self.editor.setSegmentationNode(self.segmentationNode)
        self.editor.setMasterVolumeNode(self.masterVolume)

        self.editor.setCurrentSegmentID(self.boneSegment)
        self.editor.setActiveEffectByName('Threshold')
        effect = self.editor.activeEffect()
        effect.setParameter('MinimumThreshold', '200')
        effect.setParameter('MaximumThreshold', str(scalarRange[1]))
        effect.self().onApply()

        self.tissueSegment = self.segmentationNode.GetSegmentation().AddEmptySegment('tissue', 'tissue',[1,0.2,0.46])
        self.editor.setCurrentSegmentID(self.tissueSegment)
        self.editor.setActiveEffectByName('Threshold')
        effect = self.editor.activeEffect()
        effect.setParameter('MinimumThreshold', '40')
        effect.setParameter('MaximumThreshold', '80')
        effect.self().onApply()

        self.fatSegment = self.segmentationNode.GetSegmentation().AddEmptySegment('fat', 'fat', [0, 0.9, 0])
        self.editor.setCurrentSegmentID(self.fatSegment)
        self.editor.setActiveEffectByName('Threshold')
        effect = self.editor.activeEffect()
        effect.setParameter('MinimumThreshold', '-100')
        effect.setParameter('MaximumThreshold', '-60')
        effect.self().onApply()

        self.airSegment = self.segmentationNode.GetSegmentation().AddEmptySegment('air', 'air', [0.4,0.8,1])
        self.editor.setCurrentSegmentID(self.airSegment)
        self.editor.setActiveEffectByName('Threshold')
        effect = self.editor.activeEffect()
        effect.setParameter('MinimumThreshold', str(scalarRange[0]))
        effect.setParameter('MaximumThreshold', '-150')
        effect.self().onApply()


  def selectParameterNode(self):
    # Select parameter set node if one is found in the scene, and create one otherwise
    segmentEditorSingletonTag = "SegmentEditor"
    segmentEditorNode = slicer.mrmlScene.GetSingletonNode(segmentEditorSingletonTag, "vtkMRMLSegmentEditorNode")
    if segmentEditorNode is None:
      segmentEditorNode = slicer.vtkMRMLSegmentEditorNode()
      segmentEditorNode.SetSingletonTag(segmentEditorSingletonTag)
      segmentEditorNode = slicer.mrmlScene.AddNode(segmentEditorNode)
    if self.parameterSetNode == segmentEditorNode:
      # nothing changed
      return
    self.parameterSetNode = segmentEditorNode
    self.editor.setMRMLSegmentEditorNode(self.parameterSetNode)

  def getCompositeNode(self, layoutName):
    """ use the Red slice composite node to define the active volumes """
    count = slicer.mrmlScene.GetNumberOfNodesByClass('vtkMRMLSliceCompositeNode')
    for n in xrange(count):
      compNode = slicer.mrmlScene.GetNthNodeByClass(n, 'vtkMRMLSliceCompositeNode')
      if layoutName and compNode.GetLayoutName() != layoutName:
        continue
      return compNode

  def getDefaultMasterVolumeNodeID(self):
    layoutManager = slicer.app.layoutManager()
    # Use first background volume node in any of the displayed layouts
    for layoutName in layoutManager.sliceViewNames():
      compositeNode = self.getCompositeNode(layoutName)
      if compositeNode.GetBackgroundVolumeID():
        return compositeNode.GetBackgroundVolumeID()
    # Use first background volume node in any of the displayed layouts
    for layoutName in layoutManager.sliceViewNames():
      compositeNode = self.getCompositeNode(layoutName)
      if compositeNode.GetForegroundVolumeID():
        return compositeNode.GetForegroundVolumeID()
    # Not found anything
    return None

  def onDeleteTargetFiducials(self):
    identity = vtk.vtkMatrix4x4()
    self.probeTransform.SetMatrixTransformToParent(identity)
    if self.targetFiducialList != None:
      self.targetFiducialList.RemoveAllMarkups()

  def onPlacement(self):
    if self.placeTipButton.checked:
      self.setCurrentMouseMode(self.MOUSE_MODE_PLACEMENT_TIP)
      #self.placeEntryButton.checked = 0
    elif self.placeEntryButton.checked:
      #self.placeTipButton.checked = False
      self.setCurrentMouseMode(self.MOUSE_MODE_PLACEMENT_ENTRY)
      
    else:
      self.setCurrentMouseMode(self.MOUSE_MODE_IDLE)

  def onDeleteEntryFiducials(self):
    identity = vtk.vtkMatrix4x4()
    self.probeTransform.SetMatrixTransformToParent(identity)
    if self.entryFiducialList != None:
      self.entryFiducialList.RemoveAllMarkups()

  def setCurrentMouseMode(self, mode):
    selectionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLSelectionNodeSingleton")
    interactionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLInteractionNodeSingleton")
    if mode == self.MOUSE_MODE_PLACEMENT_TIP:
      logging.debug("Switched to PLACEMENT of tip mode")
      selectionNode.SetReferenceActivePlaceNodeClassName("vtkMRMLMarkupsFiducialNode")
      selectionNode.SetActivePlaceNodeID(self.targetFiducialList.GetID())
      placeModePersistence = 0
      slicer.modules.markups.logic().StartPlaceMode(placeModePersistence)
      self.placeTipButton.checked = True
    elif mode == self.MOUSE_MODE_PLACEMENT_ENTRY:
      logging.debug("Switched to PLACEMENT of entry mode")
      selectionNode.SetReferenceActivePlaceNodeClassName("vtkMRMLMarkupsFiducialNode")
      selectionNode.SetActivePlaceNodeID(self.entryFiducialList.GetID())
      placeModePersistence = 0
      slicer.modules.markups.logic().StartPlaceMode(placeModePersistence)
    else:
      logging.debug("Switched to IDLE mode")
      interactionNode.SwitchToViewTransformMode()
      self.placeTipButton.checked = False
      self.placeEntryButton.checked = False


  def onTargetFiducialModified(self, observer, eventId):
    self.updateTransform()
    

  def onEntryFiducialModified(self, observer, eventId):
    self.updateTransform()

  def updateTransform(self):
    target = [0,0,0]
    self.targetFiducialList.GetNthFiducialPosition(0,target)
    entry = [0,0,0]
    self.entryFiducialList.GetNthFiducialPosition(0,entry)
    if target != [0,0,0] and entry != [0,0,0]:
      zVec = [target[0]-entry[0],target[1]-entry[1],target[2]-entry[2]]
      xVec = [1,0,0]
      yVec = np.cross(zVec,xVec)
      xVec = np.cross(yVec,zVec)
      zVec = zVec / np.linalg.norm(zVec)
      yVec = yVec / np.linalg.norm(yVec)
      xVec = xVec / np.linalg.norm(xVec)

      matrix = vtk.vtkMatrix4x4()
      self.probeTransform.GetMatrixTransformToParent(matrix)
      matrix.SetElement(0, 0, xVec[0])
      matrix.SetElement(1, 0, xVec[1])
      matrix.SetElement(2, 0, xVec[2])
      matrix.SetElement(0, 1, yVec[0])
      matrix.SetElement(1, 1, yVec[1])
      matrix.SetElement(2, 1, yVec[2])
      matrix.SetElement(0, 2, zVec[0])
      matrix.SetElement(1, 2, zVec[1])
      matrix.SetElement(2, 2, zVec[2])
      matrix.SetElement(0, 3, target[0])
      matrix.SetElement(1, 3, target[1])
      matrix.SetElement(2, 3, target[2])
      self.probeTransform.SetMatrixTransformToParent(matrix)
      self.updateSliceView()

  def updateSliceView(self):
    probeToWorldTransformMatrix = vtk.vtkMatrix4x4()
    self.probeTransform.GetMatrixTransformToWorld(probeToWorldTransformMatrix)
    yellowSlice = slicer.app.layoutManager().sliceWidget("Yellow")
    yellowController = yellowSlice.sliceController()
    yellowController.setSliceOffsetValue(probeToWorldTransformMatrix.GetElement(0,3))
    greenSlice = slicer.app.layoutManager().sliceWidget("Green")
    greenController = greenSlice.sliceController()
    greenController.setSliceOffsetValue(probeToWorldTransformMatrix.GetElement(1, 3))
    redSlice = slicer.app.layoutManager().sliceWidget("Red")
    redController = redSlice.sliceController()
    redController.setSliceOffsetValue(probeToWorldTransformMatrix.GetElement(2, 3))
    slicer.app.layoutManager().threeDWidget(0).threeDView().resetFocalPoint()
    slicer.app.layoutManager().threeDWidget(0).threeDView().resetCamera()

  def onProbeParameter(self):
    if self.probeModel == None:
      rfaProbeModelPath = os.path.join(os.path.dirname(__file__), 'RFAProbe.ply')
      rfaProbeModel1 = slicer.modules.models.logic().AddModel(rfaProbeModelPath)
      self.probeModel = slicer.util.getNode('RFAProbe')
      self.probeModel.GetDisplayNode().SetColor(0,1,1)
      probeDisplayNode = self.probeModel.GetDisplayNode()
      probeDisplayNode.SetSliceIntersectionVisibility(1)
      self.probeTransform = slicer.vtkMRMLTransformNode()
      self.probeTransform.SetName("ProbeTransform")
      slicer.mrmlScene.AddNode(self.probeTransform)
      self.probeModel.SetAndObserveTransformNodeID(self.probeTransform.GetID())
      exportedModelsNode = slicer.mrmlScene.AddNewNodeByClass('vtkMRMLModelHierarchyNode')
      segId = vtk.vtkStringArray()
      segId.InsertNextValue('bone')
      slicer.modules.segmentations.logic().ExportSegmentsToModelHierarchy(self.segmentationNode, segId, exportedModelsNode)
      boneModel = slicer.util.getFirstNodeByClassByName('vtkMRMLModelNode', 'bone')
      boneModelDisplay = boneModel.GetDisplayNode()
      boneModelDisplay.SetOpacity(0.5)
      self.segmentationNode.RemoveClosedSurfaceRepresentation()
      self.labelMap = slicer.mrmlScene.AddNewNodeByClass('vtkMRMLLabelMapVolumeNode')
      slicer.modules.segmentations.logic().ExportAllSegmentsToLabelmapNode(self.segmentationNode, self.labelMap)
      slicer.app.layoutManager().threeDWidget(0).threeDView().resetFocalPoint()
      slicer.app.layoutManager().threeDWidget(0).threeDView().resetCamera()
      self.segmentationNode.SetDisplayVisibility(0)
      if self.targetFiducialList == None:
        self.targetFiducialList = slicer.vtkMRMLMarkupsFiducialNode()
        self.targetFiducialList.SetName("targetLocation")
        slicer.mrmlScene.AddNode(self.targetFiducialList)
        targetDisplay = self.targetFiducialList.GetDisplayNode()
        targetDisplay.SetSelectedColor([0, 0, 1])
        targetDisplay.SetGlyphScale(5)
        targetDisplay.SetTextScale(2)
        tag = self.targetFiducialList.AddObserver(vtk.vtkCommand.ModifiedEvent, self.onTargetFiducialModified)
      if self.entryFiducialList == None:
        self.entryFiducialList = slicer.vtkMRMLMarkupsFiducialNode()
        self.entryFiducialList.SetName("entryLocation")
        slicer.mrmlScene.AddNode(self.entryFiducialList)
        entryDisplay = self.entryFiducialList.GetDisplayNode()
        entryDisplay.SetSelectedColor([0, 0, 1])
        entryDisplay.SetGlyphScale(5)
        entryDisplay.SetTextScale(2)
        tag1 = self.entryFiducialList.AddObserver(vtk.vtkCommand.ModifiedEvent, self.onEntryFiducialModified)

  def onLoadNonDICOM(self):
    loaded = slicer.util.openAddDataDialog()
    self.planVolumeSelector.setCurrentNodeIndex(0)
    slicer.app.layoutManager().threeDWidget(0).threeDView().resetCamera()
    slicer.app.layoutManager().activeMRMLThreeDViewNode().SetBoxVisible(0)
  
  def onLoadDICOM(self):
    self.dicomWidget = slicer.modules.dicom.widgetRepresentation().self()
    self.dicomWidget.detailsPopup.open()
    self.planVolumeSelector.setCurrentNodeIndex(0)
    slicer.app.layoutManager().threeDWidget(0).threeDView().resetCamera()
    slicer.app.layoutManager().activeMRMLThreeDViewNode().SetBoxVisible(0)

  def onViewMode(self, mode):
    displayLayoutManager = slicer.app.layoutManager()
    if mode == self.VIEW_MODE_TABBED_SLICE:
      displayLayoutManager.layout = slicer.vtkMRMLLayoutNode.SlicerLayoutTabbedSliceView
      self.displayTabbedSliceButton.setChecked(True)
      self.displayFourButton.setChecked(False)
      self.displayConventionalButton.setChecked(False)
      self.display3DButton.setChecked(False)
    elif mode == self.VIEW_MODE_FOUR_UP:
      displayLayoutManager.layout = slicer.vtkMRMLLayoutNode.SlicerLayoutFourUpView
      self.displayTabbedSliceButton.setChecked(False)
      self.displayFourButton.setChecked(True)
      self.displayConventionalButton.setChecked(False)
      self.display3DButton.setChecked(False)
    elif mode == self.VIEW_MODE_CONVENTIONAL:
      displayLayoutManager.layout = slicer.vtkMRMLLayoutNode.SlicerLayoutConventionalView
      self.displayTabbedSliceButton.setChecked(False)
      self.displayFourButton.setChecked(False)
      self.displayConventionalButton.setChecked(True)
      self.display3DButton.setChecked(False)
    elif mode == self.VIEW_MODE_ONE_UP_3D:
      displayLayoutManager.layout = slicer.vtkMRMLLayoutNode.SlicerLayoutOneUp3DView
      self.displayTabbedSliceButton.setChecked(False)
      self.displayFourButton.setChecked(False)
      self.displayConventionalButton.setChecked(False)
      self.display3DButton.setChecked(True)
  
  def showPythonConsole(self):
    mainWindow = slicer.util.mainWindow()
    mainWindow.on_WindowPythonInteractorAction_triggered()

  def showApplicationLog(self):
    mainWindow = slicer.util.mainWindow()
    mainWindow.on_HelpReportBugOrFeatureRequestAction_triggered()

  def showFullScreen(self):

    # We hide all toolbars, etc. which is inconvenient as a default startup setting,
    # therefore disable saving of window setup.
    settings = qt.QSettings()
    settings.setValue('MainWindow/RestoreGeometry', 'false')
    
    self.showToolbars(False)
    self.showModulePanel(False)
    self.showMenuBar(False)
    self.showFullSliceViewGui(False)
    
    mainWindow=slicer.util.mainWindow()
    mainWindow.addDockWidget(qt.Qt.LeftDockWidgetArea, self.mainMenuDockWidget)
    #broken commented out
    #mainWindow.tabifyDockWidget(self.mainMenuDockWidget)
    #mainWindow.tabifyDockWidget(self.mainMenuDockWidget)
    
    self.mainMenuDockWidget.show()
    
    #mainWindow.setTabPosition(qt.Qt.LeftDockWidgetArea, qt.QTabWidget.North)
    
    self.mainMenuDockWidget.raise_()

    mainWindow.setWindowTitle('Spinal Fusion Surgical System')
    #mainWindow.windowIcon = qt.QIcon(self.moduleDirectoryPath + '/Resources/Icons/SpineMain.png')
    #mainWindow.showFullScreen()
    mainWindow.showMaximized()
    
    # Hide slice view annotations (patient name, scale, color bar, etc.)
    import DataProbe
    dataProbeUtil=DataProbe.DataProbeLib.DataProbeUtil()
    dataProbeParameterNode=dataProbeUtil.getParameterNode()
    dataProbeParameterNode.SetParameter('showSliceViewAnnotations', '0')
    
  def onShowFullSlicerInterface(self):
    self.showToolbars(True)
    self.showModulePanel(True)    
    self.showMenuBar(True)
    self.showFullSliceViewGui(True)
    slicer.util.mainWindow().showMaximized()
    
    # Save current state
    settings = qt.QSettings()
    settings.setValue('MainWindow/RestoreGeometry', 'true')

  def showToolbars(self, show):
    for toolbar in slicer.util.mainWindow().findChildren('QToolBar'):
      toolbar.setVisible(show)

  def showModulePanel(self, show):
    modulePanelDockWidget = slicer.util.mainWindow().findChildren('QDockWidget','PanelDockWidget')[0]
    modulePanelDockWidget.setVisible(show)
    if show:
      mainWindow=slicer.util.mainWindow()
      mainWindow.tabifyDockWidget(self.mainMenuDockWidget, modulePanelDockWidget)
  
  def showMenuBar(self, show):
    for menubar in slicer.util.mainWindow().findChildren('QMenuBar'):
      menubar.setVisible(show)

  def showFullSliceViewGui(self, show):
    redSlice = slicer.app.layoutManager().sliceWidget("Red")
    self.showViewerPinButton(redSlice, show)
    yellowSlice = slicer.app.layoutManager().sliceWidget("Yellow")
    self.showViewerPinButton(yellowSlice, show)
    greenSlice = slicer.app.layoutManager().sliceWidget("Green")
    self.showViewerPinButton(greenSlice, show)
    threeDWidget = slicer.app.layoutManager().threeDWidget(0)
    self.showViewerPinButton(threeDWidget, show)

  def showViewerPinButton(self, sliceWidget, show):
    try:
      sliceControlWidget = sliceWidget.children()[1]
      pinButton = sliceControlWidget.children()[1].children()[1]
      if show:
        pinButton.show()
      else:
        pinButton.hide()
    except:
      pass      
    
 

  def onExit(self):
    slicer.util.mainWindow().close()

  

#
# VolumeClipWithModelLogic
#

class RFAPlanningLogic(ScriptedLoadableModuleLogic):
  """This class should implement all the actual
  computation done by your module.  The interface
  should be such that other python code can import
  this class and make use of the functionality without
  requiring an instance of the Widget.
  """

  def __init__(self, parent = None):
    ScriptedLoadableModuleLogic.__init__(self,parent)
    self.isSingletonParameterNode = False # allow having multiple plans in the scene








