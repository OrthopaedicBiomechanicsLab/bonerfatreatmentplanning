import os
import unittest
import vtk, qt, ctk, slicer
import numpy as np
from slicer.ScriptedLoadableModule import *
import logging

#
# RfaResults
#

class RfaResults(ScriptedLoadableModule):
  """Uses ScriptedLoadableModule base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "RFA Plan Analysis" # TODO make this more human readable by adding spaces
    self.parent.categories = ["Radiofrequency Ablation"]
    self.parent.dependencies = []
    self.parent.contributors = ["John Doe (AnyWare Corp.)"] # replace with "Firstname Lastname (Organization)"
    self.parent.helpText = """
This is an example of scripted loadable module bundled in an extension.
It performs a simple thresholding on the input volume and optionally captures a screenshot.
"""
    self.parent.helpText += self.getDefaultModuleDocumentationLink()
    self.parent.acknowledgementText = """
This file was originally developed by Jean-Christophe Fillion-Robin, Kitware Inc.
and Steve Pieper, Isomics, Inc. and was partially funded by NIH grant 3P41RR013218-12S1.
""" # replace with organization, grant and thanks.

#
# RfaResultsWidget
#

class RfaResultsWidget(ScriptedLoadableModuleWidget):
  """Uses ScriptedLoadableModuleWidget base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setup(self):
    ScriptedLoadableModuleWidget.setup(self)

    # Instantiate and connect widgets ...

    #
    # Parameters Area
    #
    parametersCollapsibleButton = ctk.ctkCollapsibleButton()
    parametersCollapsibleButton.text = "Parameters"
    self.layout.addWidget(parametersCollapsibleButton)

    # Layout within the dummy collapsible button
    parametersFormLayout = qt.QFormLayout(parametersCollapsibleButton)

    #
    # input volume selector
    #
    self.inputModelSelector = slicer.qMRMLNodeComboBox()
    self.inputModelSelector.nodeTypes = ["vtkMRMLModelNode"]
    self.inputModelSelector.selectNodeUponCreation = True
    self.inputModelSelector.addEnabled = False
    self.inputModelSelector.removeEnabled = False
    self.inputModelSelector.noneEnabled = True
    self.inputModelSelector.showHidden = False
    self.inputModelSelector.showChildNodeTypes = False
    self.inputModelSelector.setMRMLScene(slicer.mrmlScene)
    self.inputModelSelector.setToolTip( "Pick the input to the algorithm." )
    parametersFormLayout.addRow("Input Model: ", self.inputModelSelector)


    self.inputThresholdValue = qt.QDoubleSpinBox()
    self.inputThresholdValue.setValue(60.00)
    self.inputThresholdValue.setSingleStep(1.00)
    parametersFormLayout.addRow("Input Threshold Value: ", self.inputThresholdValue)

    #
    # Model visualization Button
    #
    self.threeDVisualizationButton = qt.QPushButton("3D Visualization")
    self.threeDVisualizationButton.toolTip = "Run the algorithm."
    self.threeDVisualizationButton.enabled = True
    parametersFormLayout.addRow(self.threeDVisualizationButton)

    #
    # Volume Visualization Button
    #
    self.volumeVisualizationButton = qt.QPushButton("Volume Visualization")
    self.volumeVisualizationButton.toolTip = "Show the heat distribution in the volume"
    self.volumeVisualizationButton.enabled = True
    parametersFormLayout.addRow(self.volumeVisualizationButton)

    #
    # Tissue Damage Volume Button
    #
    self.tissueDamageVolumeButton = qt.QPushButton("Calculate Tissue Damage Volume")
    self.tissueDamageVolumeButton.toolTip = "Calculate the volume of tissue damage"
    self.tissueDamageVolumeButton.enabled = True
    parametersFormLayout.addRow(self.tissueDamageVolumeButton)

    self.parameterSetNode = None
    self.segmentationNode = None
    self.masterVolume = None

    import qSlicerSegmentationsModuleWidgetsPythonQt
    self.editor = qSlicerSegmentationsModuleWidgetsPythonQt.qMRMLSegmentEditorWidget()
    self.editor.setMaximumNumberOfUndoStates(10)
    self.selectParameterNode()
    self.editor.setMRMLScene(slicer.mrmlScene)

    #
    # Volume of damage in MM
    #
    self.damage_MM = qt.QLineEdit()
    self.damage_MM.setReadOnly(True)
    parametersFormLayout.addRow("Volume of tissue damage: ", self.damage_MM)

    #
    # Dose volume histogram
    #
    self.computeDoseVolumeHistogramLabel = qt.QLabel("Compute Dose Volume Histogram")
    parametersFormLayout.addRow(self.computeDoseVolumeHistogramLabel)

    self.dvhWidget = slicer.modules.dosevolumehistogram.createNewWidgetRepresentation()
    self.dvhWidget.children()[1].collapsed = True
    self.dvhWidget.children()[5].children()[3].checked = False
    parametersFormLayout.addRow(self.dvhWidget)

    #
    # Isodose contours
    #
    self.createIsodoseLabel = qt.QLabel("Create Isodose")
    parametersFormLayout.addRow(self.createIsodoseLabel)
    defaultIsodose = slicer.util.getNode('Isodose_ColorTable_Default')
    defaultIsodose.SetColorName(0,'15')
    defaultIsodose.SetColorName(1,'30')
    defaultIsodose.SetColorName(2,'45')
    defaultIsodose.SetColorName(3,'59')
    defaultIsodose.SetColorName(4,'75')
    defaultIsodose.SetNumberOfColors(5)
    self.createIsodoseWidget = slicer.modules.isodose.createNewWidgetRepresentation()
    inputDisplay = self.createIsodoseWidget.children()[4]
    doseOnlyCheckBox = inputDisplay.children()[-1]
    doseOnlyCheckBox.checked = False
    parametersFormLayout.addRow(self.createIsodoseWidget)


    # connections
    self.threeDVisualizationButton.connect('clicked(bool)', self.onThreeDVisualizationButton)
    self.volumeVisualizationButton.connect('clicked(bool)', self.onVolumeVisualizationButton)
    self.tissueDamageVolumeButton.connect('clicked(bool)', self.onTissueDamageButton)

    # Add vertical spacer
    self.layout.addStretch(1)

  def cleanup(self):
    pass

  def onThreeDVisualizationButton(self):
    logic = RfaResultsLogic()
    logic.threeDVisualize(self.inputModelSelector.currentNode(), self.inputThresholdValue.value)
  def onVolumeVisualizationButton(self):
    logic = RfaResultsLogic()
    self.thermalVolumeNode = logic.visualizeVolume(self.inputModelSelector.currentNode())
    subjectHierarchyNode = slicer.vtkMRMLSubjectHierarchyNode().GetSubjectHierarchyNode(slicer.mrmlScene)
    subjectID = subjectHierarchyNode.CreateSubjectItem(subjectHierarchyNode.GetSceneItemID(), "patient")
    studyID = subjectHierarchyNode.CreateStudyItem(subjectID, "Name")
    volumeID = subjectHierarchyNode.GetItemByDataNode(self.thermalVolumeNode)
    subjectHierarchyNode.SetItemParent(volumeID, studyID)

  def getMasterVolume(self):
    volumeNode = slicer.util.getFirstNodeByName("ThermalDamageVolume")
    return volumeNode

  def onTissueDamageButton(self):
    value = self.inputThresholdValue.value
    print(value)
    if len(slicer.util.getNodesByClass("vtkMRMLScalarVolumeNode")) > 0:
      if self.segmentationNode == None:

        self.segmentationNode = slicer.vtkMRMLSegmentationNode()
        slicer.mrmlScene.AddNode(self.segmentationNode)
        self.segmentationNode.SetName("ThermalDamage")
        self.segmentationNode.CreateDefaultDisplayNodes()
        self.masterVolume = self.getMasterVolume()
        scalarRange = self.masterVolume.GetImageData().GetScalarRange()
        self.segmentationNode.SetReferenceImageGeometryParameterFromVolumeNode(self.masterVolume)
        self.damageSegment = self.segmentationNode.GetSegmentation().AddEmptySegment('thermalDamage', 'thermalDamage', [0.5,1,1])

        self.editorNode = slicer.mrmlScene.AddNewNodeByClass("vtkMRMLSegmentEditorNode")
        self.editor.setMRMLSegmentEditorNode(self.editorNode)
        self.editor.setSegmentationNode(self.segmentationNode)
        self.editor.setMasterVolumeNode(self.masterVolume)

        self.editor.setCurrentSegmentID(self.damageSegment)
        self.editor.setActiveEffectByName('Threshold')
        effect = self.editor.activeEffect()
        effect.setParameter('MinimumThreshold', str(self.inputThresholdValue.value))
        effect.setParameter('MaximumThreshold', str(scalarRange[1]))
        effect.self().onApply()

        exportedModelsNode = slicer.mrmlScene.AddNewNodeByClass('vtkMRMLModelHierarchyNode')
        segId = vtk.vtkStringArray()
        segId.InsertNextValue('thermalDamage')
        slicer.modules.segmentations.logic().ExportSegmentsToModelHierarchy(self.segmentationNode, segId, exportedModelsNode)
        damageModel = slicer.util.getFirstNodeByClassByName('vtkMRMLModelNode', 'thermalDamage')
        damageModelDisplay = damageModel.GetDisplayNode()
        damageModelDisplay.SetOpacity(1)
        self.segmentationNode.RemoveClosedSurfaceRepresentation()

        damageModelPolyData = damageModel.GetPolyData()
        damageModelProperties = vtk.vtkMassProperties()
        damageModelProperties.SetInputData(damageModelPolyData)
        damageModelProperties.Update()

        self.damage_MM.setText(str(np.round(damageModelProperties.GetVolume())) + ' mm')


  def selectParameterNode(self):
    # Select parameter set node if one is found in the scene, and create one otherwise
    segmentEditorSingletonTag = "SegmentEditor"
    segmentEditorNode = slicer.mrmlScene.GetSingletonNode(segmentEditorSingletonTag, "vtkMRMLSegmentEditorNode")
    if segmentEditorNode is None:
      segmentEditorNode = slicer.vtkMRMLSegmentEditorNode()
      segmentEditorNode.SetSingletonTag(segmentEditorSingletonTag)
      segmentEditorNode = slicer.mrmlScene.AddNode(segmentEditorNode)
    if self.parameterSetNode == segmentEditorNode:
      # nothing changed
      return
    self.parameterSetNode = segmentEditorNode
    self.editor.setMRMLSegmentEditorNode(self.parameterSetNode)

  def getCompositeNode(self, layoutName):
    """ use the Red slice composite node to define the active volumes """
    count = slicer.mrmlScene.GetNumberOfNodesByClass('vtkMRMLSliceCompositeNode')
    for n in xrange(count):
      compNode = slicer.mrmlScene.GetNthNodeByClass(n, 'vtkMRMLSliceCompositeNode')
      if layoutName and compNode.GetLayoutName() != layoutName:
        continue
      return compNode

  def getDefaultMasterVolumeNodeID(self):
    layoutManager = slicer.app.layoutManager()
    # Use first background volume node in any of the displayed layouts
    for layoutName in layoutManager.sliceViewNames():
      compositeNode = self.getCompositeNode(layoutName)
      if compositeNode.GetBackgroundVolumeID():
        return compositeNode.GetBackgroundVolumeID()
    # Use first background volume node in any of the displayed layouts
    for layoutName in layoutManager.sliceViewNames():
      compositeNode = self.getCompositeNode(layoutName)
      if compositeNode.GetForegroundVolumeID():
        return compositeNode.GetForegroundVolumeID()
    # Not found anything
    return None





#
# RfaResultsLogic
#

class RfaResultsLogic(ScriptedLoadableModuleLogic):
  """This class should implement all the actual
  computation done by your module.  The interface
  should be such that other python code can import
  this class and make use of the functionality without
  requiring an instance of the Widget.
  Uses ScriptedLoadableModuleLogic base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def visualizeVolume(self, modelNode):
    if modelNode != None:
      thermalVolumeNodeName = "ThermalDamageVolume"
      imageSize = [100,100,100]
      voxelType = vtk.VTK_FLOAT
      bounds = [0,0,0,0,0,0]
      modelNode.GetBounds(bounds)
      imageOrigin = [bounds[0], bounds[2], bounds[4]]
      imageSpacing = [(bounds[1]-bounds[0])/imageSize[0], (bounds[3]-bounds[2])/imageSize[1], (bounds[5]-bounds[4])/imageSize[2]]
      imageDirections = [[1,0,0], [0,1,0], [0,0,1]]
      imageData = vtk.vtkImageData()
      imageData.SetDimensions(imageSize)
      imageData.AllocateScalars(voxelType, 1)
      thermalVolumeNode = slicer.mrmlScene.AddNewNodeByClass("vtkMRMLScalarVolumeNode", thermalVolumeNodeName)
      thermalVolumeNode.SetOrigin(imageOrigin)
      thermalVolumeNode.SetSpacing(imageSpacing)
      thermalVolumeNode.SetIJKToRASDirections(imageDirections)
      thermalVolumeNode.SetAndObserveImageData(imageData)
      thermalVolumeNode.CreateDefaultDisplayNodes()
      transformer = vtk.vtkTransformFilter()
      transformer.SetInputConnection(modelNode.GetMeshConnection())
      matrixRasToIjk = vtk.vtkMatrix4x4()
      thermalVolumeNode.GetRASToIJKMatrix(matrixRasToIjk)
      transformRasToIjk = vtk.vtkTransform()
      transformRasToIjk.SetMatrix(matrixRasToIjk)
      transformer.SetTransform(transformRasToIjk)

      probe = vtk.vtkProbeFilter()
      probe.SetSourceConnection(transformer.GetOutputPort())
      probe.SetInputConnection(thermalVolumeNode.GetImageDataConnection())
      probe.Update()
      thermalVolumeNode.SetAndObserveImageData(probe.GetOutput())
      thermalVolumeNode.GetDisplayNode().SetAndObserveColorNodeID(slicer.util.getFirstNodeByName('Inferno').GetID())
      modelNode.GetDisplayNode().BackfaceCullingOff()
      slicer.util.setSliceViewerLayers(foreground=thermalVolumeNode)
      sliceLogics = slicer.app.layoutManager().mrmlSliceLogics()
      for i in range(sliceLogics.GetNumberOfItems()):
        sliceLogics.GetItemAsObject(i).FitSliceToAll()

      lm = slicer.app.layoutManager()
      sliceLogic = lm.sliceWidget('Red').sliceLogic()
      compositeNode = sliceLogic.GetSliceCompositeNode()
      compositeNode.SetForegroundOpacity(0.5)

      sliceLogic = lm.sliceWidget('Yellow').sliceLogic()
      compositeNode = sliceLogic.GetSliceCompositeNode()
      compositeNode.SetForegroundOpacity(0.5)

      sliceLogic = lm.sliceWidget('Green').sliceLogic()
      compositeNode = sliceLogic.GetSliceCompositeNode()
      compositeNode.SetForegroundOpacity(0.4)
      return thermalVolumeNode

  def threeDVisualize(self, inputModel, thresholdValue):
    model = slicer.util.getFirstNodeByName(inputModel.GetName())

    linearTransform = slicer.vtkMRMLTransformNode()
    linearTransform.SetName("MeshToRAS")
    slicer.mrmlScene.AddNode(linearTransform)
    model.SetAndObserveTransformNodeID(linearTransform.GetID())
    matrix = vtk.vtkMatrix4x4()
    linearTransform.GetMatrixTransformToParent(matrix)
    #scales to millimeters from cm that Sean uses
    matrix.SetElement(0, 0, 10)
    matrix.SetElement(1, 1, 10)
    matrix.SetElement(2, 2, 10)
    linearTransform.SetMatrixTransformToParent(matrix)

    transformLogic = slicer.vtkSlicerTransformLogic()
    transformLogic.hardenTransform(model)

    mesh = model.GetMesh()
    scalarRange = mesh.GetScalarRange()
    print("Original scalar range: ", scalarRange)
    dataSet = mesh.GetPointData()
    scalars = dataSet.GetScalars()
    numScalars = scalars.GetNumberOfValues()
    newValue = 0
    #Adjust the values below as needed for desired visual outputs and more apporpriate numbers
    for index in range(0, numScalars):
      currentScalar = scalars.GetTuple1(index)
      if currentScalar < -5:
        newValue = -5
        newScalarValue = 10**-5
      if currentScalar > 5:
        newValue = 5
        newScalarValue = 10**5
      if currentScalar >= -5 and currentScalar <= 5:
        newValue = currentScalar
        newScalarValue = 10**currentScalar
      scalars.SetTuple1(index, newScalarValue)
    scalars.Modified()
    scalarRange = mesh.GetScalarRange()
    print("New scalar range: ", scalarRange)

    # slicer.modules.models.logic().SetActiveModelNode(model)

    modelDisplayNode = model.GetDisplayNode()
    modelDisplayNode.SetActiveScalarName('scalars')
    modelDisplayNode.SetScalarVisibility(1)
    maxThreshold = modelDisplayNode.GetScalarRange()[1]
    modelDisplayNode.SetThresholdRange([thresholdValue, maxThreshold])
    modelDisplayNode.SetThresholdEnabled(1)

    modelDisplayNode.SetSliceIntersectionVisibility(1)
    modelDisplayNode.SetSliceIntersectionOpacity(0.5)






class RfaResultsTest(ScriptedLoadableModuleTest):
  """
  This is the test case for your scripted module.
  Uses ScriptedLoadableModuleTest base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setUp(self):
    """ Do whatever is needed to reset the state - typically a scene clear will be enough.
    """
    slicer.mrmlScene.Clear(0)

  def runTest(self):
    """Run as few or as many tests as needed here.
    """
    self.setUp()
    self.test_RfaResults1()

  def test_RfaResults1(self):
    """ Ideally you should have several levels of tests.  At the lowest level
    tests should exercise the functionality of the logic with different inputs
    (both valid and invalid).  At higher levels your tests should emulate the
    way the user would interact with your code and confirm that it still works
    the way you intended.
    One of the most important features of the tests is that it should alert other
    developers when their changes will have an impact on the behavior of your
    module.  For example, if a developer removes a feature that you depend on,
    your test should break so they know that the feature is needed.
    """

    self.delayDisplay("Starting the test")
    #
    # first, get some data
    #
    import urllib
    downloads = (
        ('http://slicer.kitware.com/midas3/download?items=5767', 'FA.nrrd', slicer.util.loadVolume),
        )

    for url,name,loader in downloads:
      filePath = slicer.app.temporaryPath + '/' + name
      if not os.path.exists(filePath) or os.stat(filePath).st_size == 0:
        logging.info('Requesting download %s from %s...\n' % (name, url))
        urllib.urlretrieve(url, filePath)
      if loader:
        logging.info('Loading %s...' % (name,))
        loader(filePath)
    self.delayDisplay('Finished with download and loading')

    volumeNode = slicer.util.getNode(pattern="FA")
    logic = RfaResultsLogic()
    self.assertIsNotNone( logic.hasImageData(volumeNode) )
    self.delayDisplay('Test passed!')
