Coordinate systems
==================

RFAProbe
--------

- Origin: Tip of the tool
- Unit: mm
- Axis directions: Assuming the tool is hold in hand, horizontally.
    - X axis: right.
    - Y axis: down.
    - Z axis: forward (therefore, tool is in the -Z side of the coordinate system)

![RFAProbe coordinate system](RFAProbe.png "RFAProbe")
